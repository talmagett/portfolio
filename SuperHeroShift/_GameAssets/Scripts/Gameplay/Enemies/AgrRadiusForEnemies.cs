using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgrRadiusForEnemies : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy)) {
            enemy.SetChase(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy)) {
            enemy.SetChase(false);
        }
    }
}