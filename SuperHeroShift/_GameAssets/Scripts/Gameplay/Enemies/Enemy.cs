using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[SelectionBase]
[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : Character
{
    [Header("Movement")]
    [SerializeField] private MovementData movementData;
    private float slowDuration;
    private float stunDuration;
    private NavMeshAgent agent;
    private bool isChasing=false;

    [SerializeField] private Animator animator;
    public override void OnSpawned() {
        base.OnSpawned();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = movementData.speed;
        StartCoroutine(ChasePlayer());
    }
    public IEnumerator ChasePlayer()
    {
        while (isAlive)
        {
            animator.SetBool("isMoving", isChasing);
            if (isChasing && stunDuration <= 0)
            {
                agent.isStopped = false;
                agent.SetDestination(MainManager.playerController.transform.position);
            }
            else agent.isStopped = true;
            yield return new WaitForSeconds(0.1f);
        }
    }
    public void OnFinish() {

    }
    public void Slow(float duration) {
        if (slowDuration <= 0)
        {
            if (isAlive)
            {
                slowDuration = duration;
                agent.speed = movementData.speed * 0.3f;
                ManagerUpdate.AddUpdate(SlowCounter);
            }
        }
        else
            slowDuration = (slowDuration < duration) ? duration : slowDuration;
    }
    private void SlowCounter() {
        slowDuration -= Time.deltaTime;
        if (slowDuration <= 0) { 
            agent.speed = movementData.speed;
            ManagerUpdate.RemoveUpdate(SlowCounter);
        }
    }
    public void Stun(float duration) {
        if (stunDuration <= 0)
        {
            if (isAlive)
            {
                stunDuration = duration;
                animator.speed = 0;
                agent.isStopped = true;
                ManagerUpdate.AddUpdate(StunCounter);
            }
        }
        else
            stunDuration = (stunDuration < duration) ? duration : stunDuration;
    }
    private void StunCounter() {
        stunDuration -= Time.deltaTime;
        if (stunDuration <= 0)
        {
            if (agent) {
                agent.isStopped = false;
                animator.speed = 1;
            }
            ManagerUpdate.RemoveUpdate(StunCounter);
        }
    }
    public override void Die()
    {
        isAlive = false;
        OnFinish();
        PlayerAttackRadiusSystem.RemoveFromList(this);
        ManagerUpdate.RemoveUpdate(StunCounter,SlowCounter);
        Destroy(gameObject);
    }
    public void SetChase(bool value){
        isChasing = value;
    }
}