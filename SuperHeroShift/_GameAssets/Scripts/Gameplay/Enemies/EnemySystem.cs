using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySystem : MonoBehaviour
{
    #region States
    private void OnEnable()
    {
        MainManager.GamePhasesController.OnGamePhaseChange += GamePhases;
    }
    private void OnDisable()
    {
        MainManager.GamePhasesController.OnGamePhaseChange -= GamePhases;
    }
    private void GamePhases(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.GameInit:
                break;
            case GameState.LevelInit:
                break;
            case GameState.LevelStart:
                OnLevelInit();
                break;
            case GameState.LevelFinish:
                OnLevelFinish();
                break;
            default:
                break;
        }
    }
    #endregion
    private Enemy[] enemiesList = new Enemy[] { };
    private void OnLevelInit() {
        enemiesList=FindObjectsOfType<Enemy>();
        for (int i = 0; i < enemiesList.Length; i++)
        {
            enemiesList[i].OnSpawned();
        }
    }
    private void OnLevelFinish() {
        for (int i = 0; i < enemiesList.Length; i++)
        {
            enemiesList[i].OnFinish();
        }
    }
}