using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName ="ManagerUpdate",menuName ="SO/ManagerUpdate",order =50)]
public class ManagerUpdate: ScriptableObject
{
    public bool IsUpdate = true;
    private static event Action onUpdate;
    public void Update()
    {
        if (!IsUpdate) return;
        onUpdate?.Invoke();
    }
    public static void AddUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onUpdate += item;
        }
    }
    public static void RemoveUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onUpdate -= item;
        }
    }
    public void OnDisable()
    {
        Clear();
    }
    public static void Clear()
    {
        onUpdate = null;
    }
}