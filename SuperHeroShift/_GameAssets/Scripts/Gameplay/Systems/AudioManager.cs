using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    private static AudioSource audioSource;
    private static bool hasAudio = true;
    private static bool hasVibration = true;
    private void Start()
    {
        hasAudio = (SaveSystem.GetInt("Audio", 1) == 1);
        hasVibration = (SaveSystem.GetInt("Vibration", 1) == 1);
        audioSource = GetComponent<AudioSource>();
    }
    public static void PlaySound(AudioClip clip)
    {
        if(hasAudio)
            audioSource.PlayOneShot(clip);
    }
    public void SetAudio(bool value)
    {
        hasAudio = value;
        SaveSystem.Save("Audio", hasAudio ? 1 : 0);
    }
    public void SetVibration(bool value)
    {
        hasVibration = value;
        SaveSystem.Save("Vibration", hasVibration ? 1 : 0);
    }
    public static void Vibrate(int milliseconds) {
        //if (hasVibration) ;
            //Vibration.Vibrate(milliseconds);
    }
}