using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Toolboxes
{
    public class Vector3Extension
    {
        public static float DistanceNoSqrt(Vector3 first,Vector3 second) {
            float deltaX = (first.x - second.x) * (first.x - second.x);
            float deltaY = (first.y - second.y) * (first.y - second.y);
            float deltaZ = (first.z - second.z) * (first.z - second.z);
            float deltaNoSqrt = deltaX + deltaY + deltaZ;
            return deltaNoSqrt;
        }
    }
}