using System;
using System.Collections.Generic;
using Toolboxes;
using UnityEngine;
using DG.Tweening;
public class MainManager : Singletoner<MainManager>
{
    internal static UISystem uiManager;
    internal static Camera mainCamera;
    internal static AudioManager audioManager;
    internal static PlayerController playerController;
    private ManagerUpdate managerUpdate;
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void ResetInit()
    {
        mainCamera = null;
        uiManager = null;
        audioManager = null;
        playerController = null;
    }
    void Start()
    {
        mainCamera = FindObjectOfType<Camera>();
        managerUpdate = Resources.Load<ManagerUpdate>("SO/ManagerUpdate");
        uiManager = FindObjectOfType<UISystem>();
        playerController = FindObjectOfType<PlayerController>();
        audioManager = FindObjectOfType<AudioManager>();

        GamePhasesController.SetGameState(GameState.GameInit);
        GamePhasesController.SetGameState(GameState.LevelInit);
    }
    private void Update()
    {
        if (Time.timeScale == 0) return;
        managerUpdate.Update();
    }
    private void OnDestroy()
    {
        managerUpdate.OnDisable();
    }
    private void OnDisable()
    {
        managerUpdate.OnDisable();
    }
    public static class GamePhasesController {
        private static GameState CurrentGameState;
        public static event Action<GameState> OnGamePhaseChange;
        public static void SetGameState(GameState gameState) {
            CurrentGameState = gameState;
            OnGamePhaseChange?.Invoke(CurrentGameState);
        }
    }
    public class CameraController
    {
        public static void CameraShake(float duration,float strength,int vibrato) {
            mainCamera.DOShakePosition(duration, strength, vibrato);
        }
    }
}
public enum GameState { 
    GameInit,LevelInit, LevelStart, LevelFinish
}