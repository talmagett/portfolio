using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class AreaTrigger : MonoBehaviour
{
    [SerializeField] private string _checkingTag;
    [SerializeField] private UnityEvent _onEnter;
    [SerializeField] private UnityEvent _onExit;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(_checkingTag)) _onEnter?.Invoke();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_checkingTag)) _onExit?.Invoke();
    }
}