using UnityEngine;
[System.Serializable]
public abstract class Character: MonoBehaviour{
    public float maxHP;
    internal float health;
    internal bool isAlive=true;
    public virtual void OnSpawned() {
        health = maxHP;
    }
    public virtual void TakeDamage(float value)
    {
        health -= value;
        if (health <= 0) {
            Die();
        }
    }
    public virtual void Die() {
    //    Destroy(gameObject);// gameObject.SetActive(false);
    }
}