using System.Linq;
using Toolboxes;
using UnityEngine;

public abstract class HeroLogic : MonoBehaviour
{
    public Bullet bullet;
    public float attackInterval;
    public Transform leftGunPosition;
    public Transform rightGunPosition;
    public Transform leftArm;
    public Transform rightArm;
    public Transform leftAim;
    public Transform rightAim;
    internal Transform nearestLeftEnemy;
    internal Transform nearestRightEnemy;
    internal Transform NearestEnemy(Transform basePos)
    {
        if (!PlayerAttackRadiusSystem.isEnemyNearby)
        {
            return null;
        }
        else
        {
        check:
            Enemy target = PlayerAttackRadiusSystem.Enemies.OrderBy(t => Vector3Extension.DistanceNoSqrt(basePos.position, t.transform.position)).FirstOrDefault();
            if (target == null)
            {
                PlayerAttackRadiusSystem.RemoveFromList(target);
                goto check;
            }
            return target.transform;
        }
    }

    public void Rotations()
    {
        if (PlayerAttackRadiusSystem.isEnemyNearby)
        {
            if (nearestLeftEnemy && nearestRightEnemy)
            {
                //--������� ���
                Vector3 lookDirLeft = nearestLeftEnemy.position - transform.position;
                Vector3 lookDirRight = nearestRightEnemy.position - transform.position;

                Vector3 lookDir = Vector3.zero;
                //--������� �����
                if (nearestRightEnemy == nearestLeftEnemy)//���� ����
                {
                    lookDir = nearestLeftEnemy.position - transform.position;
                }
                else //������ �����
                {
                    // lookDir = (lookDirLeft+lookDirRight).normalized;
                    lookDir = ((nearestLeftEnemy.position - transform.position) + (nearestRightEnemy.position - transform.position)).normalized;
                }
                lookDir.y = 0;
                transform.rotation = Quaternion.LookRotation(lookDir);
                float angle = 90 - (Vector3.Angle(lookDirLeft, lookDirRight) / 2);
                leftArm.localRotation = Quaternion.RotateTowards(leftArm.localRotation, Quaternion.Euler(-20, 0, -angle), 500f * Time.deltaTime);
                rightArm.localRotation = Quaternion.RotateTowards(rightArm.localRotation, Quaternion.Euler(-20, 0, angle), 500f*Time.deltaTime);

                Debug.DrawLine(leftArm.position, leftArm.position + lookDirLeft, Color.red, 0.05f);
                Debug.DrawLine(rightArm.position, rightArm.position + lookDirRight, Color.red, 0.05f);
                Debug.DrawLine(transform.position, transform.position + (lookDir), Color.red, 0.05f);
            }
        }
        else
        {
            transform.eulerAngles = new Vector3(0, Mathf.Atan2(UISystem.JoystickDirection.x, UISystem.JoystickDirection.y) * 180 / Mathf.PI, 0);
            leftArm.localRotation = Quaternion.RotateTowards(leftArm.localRotation, Quaternion.Euler(-20, 0, 0), 500f*Time.deltaTime);
            rightArm.localRotation = Quaternion.RotateTowards(rightArm.localRotation, Quaternion.Euler(-20, 0, 0), 500f * Time.deltaTime);
        }
    }
    public abstract void Attack();
    public abstract void Active();
}