using UnityEngine;
public class PlayerAttackSystem : MonoBehaviour
{
    public GameObject ultReadyObject;
    private static GameObject ultReadyObj;
    private HeroLogic currentHeroLogic;
    private static float AttackCounter = 0;
    private static float ActiveCounter = 0;
    internal static Transform nearestEnemy;
    public void LevelInit()
    {
        ManagerUpdate.AddUpdate(Attack);
        AttackCounter = 0;
        ActiveCounter = 1f;
        ultReadyObj = MainManager.playerController.GetComponent<PlayerAttackSystem>().ultReadyObject;
    }
    public void SetHero(HeroLogic heroLogic)
    {
        currentHeroLogic = heroLogic;
        currentHeroLogic.gameObject.SetActive(true);
    }
    public static void SetSpellCounter(float value) { 
        ActiveCounter = value;
        //ultReadyObj.SetActive(false);
    }
    private void Attack()
    {
        if (ActiveCounter <= 0)
        {
           // ultReadyObj.SetActive(true);
#if UNITY_ANDROID
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    currentHeroLogic.Active();
                    AttackCounter = 0.5f;
                }
            }
#endif
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0))
            {
                currentHeroLogic.Active();
                AttackCounter = 0.5f;
            }
#endif
        }
        else {
            ActiveCounter -= Time.deltaTime;
        }
        currentHeroLogic.Rotations();
        if (AttackCounter <= 0)
        {
            currentHeroLogic.Attack();
            AttackCounter = currentHeroLogic.attackInterval;
        }
        else
        {
            AttackCounter -= Time.deltaTime;
        }
    }
}