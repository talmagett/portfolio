using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketMan : HeroLogic
{
    public override void Active()
    {
        PlayerAttackSystem.SetSpellCounter(0.5f);
        for (int i = 0; i < 36; i++)
        {
            Instantiate(bullet, transform.position, Quaternion.Euler(0, i * 10, 0));
        }
    }
    public override void Attack()
    {
        nearestLeftEnemy = NearestEnemy(leftAim);
        nearestRightEnemy = NearestEnemy(rightAim);

        if (nearestLeftEnemy && nearestRightEnemy)
        {
            Transform bulletTemp = Instantiate(bullet, leftGunPosition.position, Quaternion.identity).transform;
            Transform bulletTemp2 = Instantiate(bullet, rightGunPosition.position, Quaternion.identity).transform;

            var lookDirL = nearestLeftEnemy.position - bulletTemp.position;
            var lookDirR = nearestRightEnemy.position - bulletTemp2.position;

            lookDirL.y = 0;
            lookDirR.y = 0;

            bulletTemp.rotation = Quaternion.LookRotation(lookDirL);
            bulletTemp2.rotation = Quaternion.LookRotation(lookDirR);
        }
    }
}