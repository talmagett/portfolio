using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IronManRocket : Bullet
{
    [SerializeField] GameObject explosion;
    [SerializeField] float explosionRadius;
    [SerializeField] LayerMask layerMask;
    protected override IEnumerator Start()
    {
        yield return new WaitForSeconds(lifeTime);
        Explosion();
    }
    private void Explosion() {
        explosion.SetActive(true);
        explosion.transform.SetParent(null);
        Collider[] hits = Physics.OverlapSphere(transform.position, explosionRadius, layerMask);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].TryGetComponent(out Enemy enemy)) {
                enemy.TakeDamage(bulletData.damage);
            }
        }
        Destroy(gameObject);
    }
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Character character))
        {
            GetComponent<Collider>().enabled = false;
            if (isDestroyOnHit)
            {
                Explosion();
            }
            //PoolSystem
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}