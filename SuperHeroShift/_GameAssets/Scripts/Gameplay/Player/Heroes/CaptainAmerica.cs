using System.Collections.Generic;
using UnityEngine;
public class CaptainAmerica : HeroLogic
{
    [SerializeField] private CaptainAmericaShield shield;
    [SerializeField] private Transform shieldParent;
    [SerializeField] private float shieldDamage;
    public override void Active()
    {
        if (PlayerAttackRadiusSystem.Enemies.Count > 0)
        {
            PlayerAttackSystem.SetSpellCounter(99f);
            List<Enemy> enemies = new List<Enemy>();
            foreach (var item in PlayerAttackRadiusSystem.Enemies)
            {
                enemies.Add(item);
            }
            shield.Throw(shieldParent, enemies, shieldDamage);
        }
    }
    public override void Attack()
    {
        nearestLeftEnemy = NearestEnemy(leftAim);
        nearestRightEnemy = NearestEnemy(rightAim);

        if (nearestLeftEnemy && nearestRightEnemy)
        {
            Transform bulletTemp = Instantiate(bullet, leftGunPosition.position, Quaternion.identity).transform;
            Transform bulletTemp2 = Instantiate(bullet, rightGunPosition.position, Quaternion.identity).transform;

            var lookDirL = nearestLeftEnemy.position - bulletTemp.position;
            var lookDirR = nearestRightEnemy.position - bulletTemp2.position;

            lookDirL.y = 0;
            lookDirR.y = 0;

            bulletTemp.rotation = Quaternion.LookRotation(lookDirL);
            bulletTemp2.rotation = Quaternion.LookRotation(lookDirR);
        }
    }
}