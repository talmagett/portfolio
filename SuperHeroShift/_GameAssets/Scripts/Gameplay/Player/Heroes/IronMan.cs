using UnityEngine;
public class IronMan : HeroLogic
{
    public override void Active()
    {
        PlayerAttackSystem.SetSpellCounter(3f);
        for (int i = 0; i < 36; i++)
        {
            Instantiate(bullet, transform.position+Vector3.up, Quaternion.Euler(0, i * 10, 0));
        }
    }
    public override void Attack()
    {
        nearestLeftEnemy = NearestEnemy(leftAim);
        nearestRightEnemy = NearestEnemy(rightAim);

        if (nearestLeftEnemy && nearestRightEnemy)
        {
            Transform bulletTemp = Instantiate(bullet, leftGunPosition.position, Quaternion.identity).transform;
            Transform bulletTemp2 = Instantiate(bullet, rightGunPosition.position, Quaternion.identity).transform;

            var lookDirL = nearestLeftEnemy.position - bulletTemp.position;
            var lookDirR = nearestRightEnemy.position - bulletTemp2.position;

            lookDirL.y = 0;
            lookDirR.y = 0;

            bulletTemp.rotation = Quaternion.LookRotation(lookDirL);
            bulletTemp2.rotation = Quaternion.LookRotation(lookDirR);
        }
    }
}