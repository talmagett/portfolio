using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Toolboxes;
public class CaptainAmericaShield : MonoBehaviour
{
    private Transform myParent;
    private List<Enemy> enemies = new List<Enemy>();
    private float shieldDamage;
    private int counts = 0;
    private float speed=15f;
    public void Throw(Transform myParent, List<Enemy> enemies,float shieldDamage) {
        this.enemies.Clear();
        this.myParent = myParent;
        this.enemies = enemies;
        this.shieldDamage = shieldDamage;
        transform.SetParent(null);
        transform.DOLocalRotate(new Vector3(270, 0, 0), 0.5f);
        counts = 0;
        ManagerUpdate.AddUpdate(Move);

    }
    private void Move() {

        if (counts > 10 || counts >= enemies.Count)
        {
            transform.position = Vector3.MoveTowards(transform.position, myParent.position, speed * Time.deltaTime);
            if (Vector3Extension.DistanceNoSqrt(transform.position, myParent.position) <= 0.1f)
            {
                transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, new Vector3(180, 0, 0), 1); 
                transform.SetParent(myParent);
                transform.DOLocalMove(new Vector3(0,0,-0.25f),0.2f);
                transform.DOLocalRotate(new Vector3(180,0,0),0.2f);
                PlayerAttackSystem.SetSpellCounter(0.5f);
                ManagerUpdate.RemoveUpdate(Move);
            }
        }
        else {
            if (enemies[counts])
            {
                transform.position = Vector3.MoveTowards(transform.position, enemies[counts].transform.position, speed * Time.deltaTime);
                if (Vector3Extension.DistanceNoSqrt(transform.position, enemies[counts].transform.position) <= 0.1f)
                {
                    enemies[counts].TakeDamage(shieldDamage);
                    enemies[counts].Stun(1f);
                    counts++;
                }
            }
            else
            {
                counts++;
            }
        }
    }
}
