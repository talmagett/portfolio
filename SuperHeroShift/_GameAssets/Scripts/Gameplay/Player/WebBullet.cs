using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebBullet : Bullet
{
    [SerializeField] private AutoDestruct web;
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Character character))
        {
            character.TakeDamage(bulletData.damage);
            if (isDestroyOnHit)
            {
                web.gameObject.SetActive(true);
                web.transform.SetParent(null);
                Destroy(gameObject);
                if (other.TryGetComponent(out Enemy enemy)) {
                    enemy.Slow(0.4f);
                }
                StopCoroutine(Start());
            }
            //PoolSystem
        }
    }
}