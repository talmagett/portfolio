using System.Collections.Generic;
using UnityEngine;
public class PlayerAttackRadiusSystem : MonoBehaviour
{
    private static List<Enemy> enemiesList = new List<Enemy>();
    public static List<Enemy> Enemies => enemiesList;
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void ResetInit()
    {
        enemiesList.Clear();
    }
    internal static bool isEnemyNearby => enemiesList.Count > 0;
    public static void RemoveFromList(Enemy enemy)
    {
        enemiesList.Remove(enemy);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            enemiesList.Add(enemy);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            enemiesList.Remove(enemy);
        }
    }
}