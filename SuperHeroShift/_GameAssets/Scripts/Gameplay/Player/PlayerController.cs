using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerController : Character
{
    [SerializeField] private HeroesSystem heroesSystem;
    private PlayerAttackSystem attackSystem;
    #region States
    private void OnEnable()
    {
        MainManager.GamePhasesController.OnGamePhaseChange += GamePhases;
    }
    private void OnDisable()
    {
        MainManager.GamePhasesController.OnGamePhaseChange -= GamePhases;
    }
    private void GamePhases(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.GameInit:
                Init();
                break;
            case GameState.LevelInit:
                break;
            case GameState.LevelStart:
                attackSystem.LevelInit();
                break;
            case GameState.LevelFinish:
                break;
            default:
                break;
        }
    }
    #endregion
    private void Init() {
        attackSystem = GetComponent<PlayerAttackSystem>();
        ChooseHero(1);
    }
    public void ChooseHero(int index) {
        heroesSystem.DisableHeroes();
        attackSystem.SetHero(heroesSystem.selectedHeroes[index]);
    }
    [System.Serializable]
    public class HeroesSystem {
        public List<HeroLogic> allHeroes = new List<HeroLogic>();
        public HeroLogic[] selectedHeroes = new HeroLogic[3];
        public void DisableHeroes() {
            for (int i = 0; i < selectedHeroes.Length; i++)
            {
                selectedHeroes[i].gameObject.SetActive(false);
            }
        }
    }
}