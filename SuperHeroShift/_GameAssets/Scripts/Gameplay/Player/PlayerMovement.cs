using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] CharacterController characterController;
    [SerializeField] MovementData movementData;
    private void OnEnable()
    {
        ManagerUpdate.AddUpdate(Movement);
    }
    private void OnDisable()
    {
        ManagerUpdate.RemoveUpdate(Movement);
    }
    void Movement() {
        characterController.Move(new Vector3( UISystem.JoystickDirection.x,0, UISystem.JoystickDirection.y)* movementData.speed * Time.deltaTime);
    }
}