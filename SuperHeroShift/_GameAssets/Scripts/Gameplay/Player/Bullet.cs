using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] protected BulletData bulletData;
    [SerializeField] protected bool isDestroyOnHit;
    [SerializeField] protected float lifeTime;
    public virtual void OnSpawn() {
    }
    protected virtual IEnumerator Start(){
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }
    protected void OnEnable()
    {
        ManagerUpdate.AddUpdate(Move);
    }
    protected void OnDisable()
    {
        ManagerUpdate.RemoveUpdate(Move);
    }
    protected void Move() {
        transform.position += transform.forward * bulletData.speed*Time.deltaTime;
    }
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Character character)) {
            character.TakeDamage(bulletData.damage);
            if (isDestroyOnHit) { 
                Destroy(gameObject);
                StopCoroutine(Start());
            }
            //PoolSystem
        }
    }
}
[System.Serializable]
public struct BulletData {
    public bool isBelongsToPlayer;
    public float damage;
    public float speed;
}