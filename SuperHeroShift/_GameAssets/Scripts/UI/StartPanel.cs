using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StartPanel : MonoBehaviour
{
    public void StartGame() {
        MainManager.uiManager.SetState(GameState.LevelStart);
        gameObject.SetActive(false);
    }
}