using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISystem : MonoBehaviour
{
    private static VariableJoystick joystick;
    internal static Vector2 JoystickDirection=> joystick.Direction;
    #region phases
    private void OnEnable()
    {
        MainManager.GamePhasesController.OnGamePhaseChange += GamePhases;
    }
    private void OnDisable()
    {
        MainManager.GamePhasesController.OnGamePhaseChange -= GamePhases;
    }
    private void GamePhases(GameState gameState) {
        switch (gameState)
        {
            case GameState.GameInit:
                Init();
                break;
            case GameState.LevelInit:
                break;
            case GameState.LevelStart:
                break;
            case GameState.LevelFinish:
                break;
            default:
                break;
        }
    }
    #endregion
    private void Init()
    {
        joystick = FindObjectOfType<VariableJoystick>();
    }
    public void SetState(GameState gameState) {
        MainManager.GamePhasesController.SetGameState(gameState);
    }
    internal static void ChangeHero(int index) {
        MainManager.playerController.ChooseHero(index);
    }
}