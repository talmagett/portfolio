using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHeroChoose : MonoBehaviour
{
    [SerializeField] private Button[] buttons=new Button[] { };
    [SerializeField] private GameObject[] imagesRamka=new GameObject[] { };
    public void ChooseHero(int index) {
        for (int i = 0; i < buttons.Length; i++)
        {
            if(i!= index) imagesRamka[i].SetActive(false);
            else imagesRamka[i].SetActive(true);
        }
        UISystem.ChangeHero(index);
    }
}