using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LevelTask",menuName = "ScriptableObjects/LevelTask")]
public class LevelTasks : ScriptableObject
{
	//public int BaseGold;
	public List<Order> Orders = new List<Order>();
	public List<Product> ProductsInLevel;
}
[System.Serializable]
public class Order
{
	public Product OrderingProduct;
	public int Number;
	public int GoldReward;
	internal bool isCompleted;
}