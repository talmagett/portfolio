using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLookToCamera : MonoBehaviour
{
    void Start()
    {
        GetComponent<RectTransform>().eulerAngles = new Vector3(60,EntitySystem.CameraMain.transform.eulerAngles.y,0);
    }
}