using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class BuyingPlace : MonoBehaviour
{
    [SerializeField] private AudioClip _buyingSound;
    [SerializeField] private Building _buildingBuilding;
    [SerializeField] private int _cost;
    [SerializeField] private TMP_Text _buildingNameText;
    [SerializeField] private Image _requementProductIcon;
    [SerializeField] private Image _producingProductIcon;
    [SerializeField] private Button _buyingButton;
    [SerializeField] private TMP_Text _costText;
    void Start()
    {
        _buildingNameText.text = _buildingBuilding.name;
        _requementProductIcon.sprite = _buildingBuilding.RequeringProduct.Icon;
        _producingProductIcon.sprite = _buildingBuilding.ProducingProduct.Icon;
        _costText.text = _cost.ToString();
    }
    public void BuyBuilding() {
        _buildingBuilding.gameObject.SetActive(true);
        EntitySystem.Pay(_cost);
        gameObject.SetActive(false);
        AudioManager.PlaySound(_buyingSound);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _buyingButton.interactable = (EntitySystem.GetMoneyValue() >= _cost);
        }
    }
}