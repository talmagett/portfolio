using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelData : MonoBehaviour
{
    [SerializeField] private LevelTasks _levelTask;
    public LevelTasks CurrentLevelTask => _levelTask;

	private List<SowingField> _sowingFields = new List<SowingField>();
	private List<LandPlot> _landPlots = new List<LandPlot>();
	private List<Building> _buildings = new List<Building>();
	private Market market;
	public void Init()
	{
		_sowingFields = FindObjectsOfType<SowingField>(true).ToList();
		_landPlots = FindObjectsOfType<LandPlot>(true).ToList();
		_buildings = FindObjectsOfType<Building>(true).ToList();
		market = FindObjectOfType<Market>();
		foreach (var field in _sowingFields)
		{
			field.Init();
		}
		foreach (var item in _landPlots)
		{
			item.Init();
		}
		foreach (var item in _buildings)
		{
			item.Init();
		}
		market.Init();
	}
}