using DG.Tweening;
using System.Collections;
using UnityEngine;
public class Product : MonoBehaviour
{
    public string Name;
    public Sprite Icon;
    [SerializeField] private AudioClip[] _pickupSounds = new AudioClip[] { };
    [SerializeField] private AudioClip[] _giveToSounds = new AudioClip[] { };
    [SerializeField] private AudioClip[] _generatingSounds=new AudioClip[] { };
    private Collider _collider;
    public void Starter()
    {
        _collider = GetComponent<Collider>();
    }
    public void Generating(Transform target) {
        _collider.enabled = false;
        transform.SetParent(target);
        Vector3 randPos = Random.insideUnitSphere*0.2f;
        randPos.y = 0;
        AudioManager.PlaySound(_generatingSounds[Random.Range( 0,_generatingSounds.Length)]);
        transform.DOLocalJump(randPos, 1, 1, 0.5f).OnComplete(() => _collider.enabled = true);
    }
    public void PickupFarmer(Transform farmerTransform)
    {
        _collider.enabled = false;
        transform.SetParent(farmerTransform);
        AudioManager.PlaySound(_pickupSounds[Random.Range(0, _pickupSounds.Length)]);
        transform.DOScale(Vector3.one*2,0.5f);
        transform.DOLocalJump(Vector3.zero, 2, 1, 0.5f).OnComplete(()=> gameObject.SetActive(false));
        AudioManager.Vibrate(50);
    }
    public void GiveTo(Transform target)
    {
        gameObject.SetActive(true);
        transform.SetParent(target);
        AudioManager.PlaySound(_giveToSounds[Random.Range(0, _giveToSounds.Length)]);
        transform.DOScale(Vector3.one, 0.5f);
        transform.DOLocalJump(Vector3.zero, 1, 1, 0.5f).OnComplete(() => PoolSystem.PutToPull(this));
    }
}