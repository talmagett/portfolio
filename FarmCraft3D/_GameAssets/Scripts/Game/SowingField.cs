using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SowingField : MonoBehaviour
{
    private Plant[] plants;
    public void Init() {
        plants = transform.GetComponentsInChildren<Plant>();
        for (int i = 0; i < plants.Length; i++)
		{
            plants[i].Init();
		}
    }
}