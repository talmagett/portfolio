using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
public class Market : MonoBehaviour
{
    [SerializeField] private ParticleSystem _moneyEffect;
    [SerializeField] private DOTweenAnimation _punchAnimation;
    [SerializeField] private AudioClip _rewardSound;
    [SerializeField] private float _timeBetweenTakeItem;
    private bool _isPlayerInside;
    private LevelTasks _levelTasks;
    
    private List<string> _ordersNamesList = new List<string>();
    public Dictionary<string, int> _requiredProducts = new Dictionary<string, int>();
    public void Init()
    {
        _levelTasks = EntitySystem.s_LevelTasks;
        for (int i = 0; i < _levelTasks.Orders.Count; i++)
        {
            _ordersNamesList.Add(_levelTasks.Orders[i].OrderingProduct.Name);
            _requiredProducts.Add(_ordersNamesList[i], _levelTasks.Orders[i].Number);
        }
        foreach (var order in _levelTasks.Orders)
        {
            EntitySystem.UIOrderManager.AddOrderToList(order);
        }
    }
    public IEnumerator TakeByOneItemToBuilding()
    {
        List<string> playerBelongsProducts = new List<string>();
        for (int i = 0; i < _requiredProducts.Count; i++)
        {
            if (FarmerController.HasPlayerItem(_ordersNamesList[i])) {
                playerBelongsProducts.Add(_ordersNamesList[i]);
            }
        }
        while (playerBelongsProducts.Count>0 && _isPlayerInside)
        {
            if (FarmerController.HasPlayerItem(playerBelongsProducts[0]))
            {
                FarmerController.GetProduct(playerBelongsProducts[0]).GiveTo(transform);
                _requiredProducts[playerBelongsProducts[0]]--;
                EntitySystem.UIOrderManager.ChangeNumber(playerBelongsProducts[0], _requiredProducts[playerBelongsProducts[0]]);
                if (_requiredProducts[playerBelongsProducts[0]] == 0)
                {
                    OrderComplete(playerBelongsProducts[0]);
                    AudioManager.PlaySound(_rewardSound);
                    playerBelongsProducts.RemoveAt(0);
                }
                _punchAnimation.DORestart();
                yield return new WaitForSeconds(_timeBetweenTakeItem);
            }
            else {
                playerBelongsProducts.RemoveAt(0);
            }
        }
    }
    private void OrderComplete(string productName)
    {
        _ordersNamesList.Remove(productName);
        _requiredProducts.Remove(productName);
        _moneyEffect.Play();
        foreach (var order in _levelTasks.Orders)
        {
            if (order.OrderingProduct.Name == productName)
            {
                EntitySystem.Singleton.AddMoney(order.GoldReward);
                break;
            }
        }
        if (_ordersNamesList.Count <= 0)
            EntitySystem.WinGame();
        //_customerAnimator.SetTrigger("Happy");
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _isPlayerInside = true;
            StartCoroutine(TakeByOneItemToBuilding());
        }
    }
    public void OnTriggerExit(Collider other)
    {
        _isPlayerInside = false;
    }
}