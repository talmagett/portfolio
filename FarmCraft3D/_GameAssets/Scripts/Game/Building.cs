using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class Building : MonoBehaviour
{
    [EnumToggleButtons]
    [SerializeField] private AnimationType _animationType;
    internal enum AnimationType
    {
        dotween, particle, animation
    }
    [ShowIf("_animationType", AnimationType.dotween)]
    [SerializeField] private DOTweenAnimation _producingDotweenAnimation;
    [ShowIf("_animationType", AnimationType.particle)]
    [SerializeField] private ParticleSystem _producingParticleEffect;
    [ShowIf("_animationType", AnimationType.animation)]
    [SerializeField] private Animator _producingAnimator;
    [SerializeField] private float _actionDelay;
    [SerializeField] private float _timeBetweenTakeItem;
    [SerializeField] private Product _requeringItem;
    internal Product RequeringProduct => _requeringItem;
    [SerializeField] private Product _producingItem;
    internal Product ProducingProduct => _producingItem;
    [SerializeField] private Transform _generatingPointTransform;
    [SerializeField] private DOTweenAnimation _punchAnimation;
    [Header("Canvas")]
    [Space]
  //  [SerializeField] private Transform _localCanvas;
    [SerializeField] private Image _requeringItemIcon;
    [SerializeField] private Image _producingItemIcon;
    private int _producingCount = 0;

    private bool _isPlayerInside;
    public void Init() {
        _requeringItemIcon.sprite = _requeringItem.Icon;
        _producingItemIcon.sprite = _producingItem.Icon;
        //_localCanvas.eulerAngles = new Vector3(60,EntitySystem.CameraMain.transform.eulerAngles.y,0);
        ProducingAnimationControl();
    }
    public void ProducingAnimationControl()
    {
        if (_producingCount > 0)
        {
            switch (_animationType)
            {
                case AnimationType.particle: _producingParticleEffect.Play(); break;
                case AnimationType.dotween: _producingDotweenAnimation.tween.Play(); break;
                case AnimationType.animation: _producingAnimator.speed = 1; break;
            }
        }
        else
        {
            switch (_animationType)
            {
                case AnimationType.particle: _producingParticleEffect.Stop(); break;
                case AnimationType.dotween: _producingDotweenAnimation.tween.Pause(); break;
                case AnimationType.animation: _producingAnimator.speed = 0; break;
            }
        }
    }
    public void Produce()
    {
        PoolSystem.GetFromPool(_producingItem.Name, transform.position, Quaternion.identity,transform).Generating(_generatingPointTransform);
        _producingCount--;
        ProducingAnimationControl();
    }
    public IEnumerator TakeByOneItemToBuilding()
    {
        while (FarmerController.HasPlayerItem(_requeringItem.Name) && _isPlayerInside)
        {
            Product product = FarmerController.GetProduct(_requeringItem.Name);
            product.GiveTo(transform);
            Invoke("Produce", _actionDelay);
            _producingCount++;
            ProducingAnimationControl();
            _punchAnimation.DORestart();
            yield return new WaitForSeconds(_timeBetweenTakeItem);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _isPlayerInside = true;
            StartCoroutine(TakeByOneItemToBuilding());
        }
    }
    public void OnTriggerExit(Collider other)
    {
        _isPlayerInside = false;
    }
}