using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class UIOrder : MonoBehaviour
{
    [HideInInspector]
    public string Name;
    public Image ProductIcon;
    public TMP_Text NumberOfProduct;
    public GameObject CheckMark;
    public void SetData(string name,Sprite icon,int number) {
        Name = name;
        ProductIcon.sprite= icon;
        NumberOfProduct.text = "x" + number;
    }
    public void ChangeNumber(int number) {
        NumberOfProduct.text = "x" + number;
        NumberOfProduct.transform.DORewind();
        NumberOfProduct.transform.DOPunchScale(new Vector3(0.7f,0.7f,0.7f), 0.2f);

        if (number <= 0) {
            NumberOfProduct.gameObject.SetActive(false);
            CheckMark.SetActive(true);
        }
    }
}