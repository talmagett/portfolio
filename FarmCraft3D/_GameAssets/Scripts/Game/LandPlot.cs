using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LandPlot : MonoBehaviour
{
    [SerializeField] private TMP_Text _creatingObjectNameText;
    [SerializeField] private GameObject _creatingObject;
    [SerializeField] private GameObject _popUpWindow;
    [SerializeField] private int _price;
    [SerializeField] private Button _buyButton;
    [SerializeField] private TMP_Text _priceText;
    public void Init()
    {
        _priceText.text = "$" + _price;
        _creatingObjectNameText.text=_creatingObject.name;
    }
    private void OnEnable()
    {
        EntitySystem.onMoneyChange += OnMoneyChangeButton;
    }
    private void OnDisable()
    {
        EntitySystem.onMoneyChange -= OnMoneyChangeButton;
    }
    private void OnMoneyChangeButton()
    {
        _buyButton.interactable = _price <= EntitySystem.GetMoneyValue();

    }
    public void BuyObject()
    {
        EntitySystem.Pay(_price);
        _creatingObject.SetActive(true);
        _creatingObject.transform.SetParent(transform.parent);
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _popUpWindow.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _popUpWindow.SetActive(false);
        }
    }
}