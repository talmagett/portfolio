using DG.Tweening;
using System.Collections;
using UnityEngine;
[SelectionBase]
public class Plant : MonoBehaviour
{
    [Header("PlantVisual")]
    [SerializeField] private Transform _plantTransform;
    [SerializeField] private bool isChangeScale;
    [SerializeField] private Vector3 _basePos;
    [SerializeField] private ParticleSystem _plantMowEffect;
    [SerializeField] private ParticleSystem _plantReadyEffect;
    [Space]
    [Header("PlantLogic")]
    [SerializeField] private AudioClip _harvestSound;
    [SerializeField] private bool _isRegrow=true;
    [SerializeField] private bool _isFillOnStart;
    [SerializeField] private float _growTime;
    [SerializeField] private Product _generatingProduct;
    [SerializeField] private Transform _creatingPointTransform;

    private Vector3 _lastPos;
    private Vector3 _baseScale;
    private Vector3 _changingScale;
    private Vector3 _punchingScale;
    private bool _isRipen;
    public void Init()
    {
        _lastPos = _plantTransform.localPosition;
        _baseScale = _plantTransform.localScale;
        _changingScale = _plantTransform.localScale * 0.1f;
        _punchingScale = _plantTransform.localScale * 0.5f;
        if (_isFillOnStart)
        {
            Ripen();
            _plantTransform.localPosition = _lastPos;
        }
        else
        {
            _plantTransform.localPosition = _basePos;
            StartCoroutine(Grow());
        }
    }
    private IEnumerator Grow()
    {
        _plantTransform.localPosition = _basePos;
        if (isChangeScale)
            _plantTransform.localScale = _changingScale;
        if (_isRegrow)
        {
            yield return new WaitForSeconds(0.75f);
            if (isChangeScale)
                _plantTransform.DOScale(_baseScale, _growTime - 1);
            _plantTransform.DOLocalMove(_lastPos, _growTime - 1).SetEase(Ease.Linear).OnComplete(() =>
            {
                _plantTransform.DOPunchScale((_punchingScale), 0.25f, 1, 20f).OnComplete(() =>
                            Ripen()
                );
            });
        }
    }
    private void Ripen()
    {
        _isRipen = true;
        _plantReadyEffect.Play();
    }
    public void Harvest()
    {
        _isRipen = false;

        PoolSystem.GetFromPool(_generatingProduct.Name, _creatingPointTransform.position, Quaternion.identity,transform);
        AudioManager.PlaySound(_harvestSound);
        _plantMowEffect.Play();
        _plantReadyEffect.Stop();
        StartCoroutine(Grow());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_isRipen)
            {
                if (other.TryGetComponent(out FarmerController farmerController))
                {
                    farmerController.FarmerAnimatorController.Harvest();
                }
                Harvest();
            }
        }
    }
}