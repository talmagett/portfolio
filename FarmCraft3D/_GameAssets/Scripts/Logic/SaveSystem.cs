using UnityEngine;
public class SaveSystem
{
    public static void Save(string savingName, object value)
    {
        if (value is float)
            PlayerPrefs.SetFloat(savingName, (float)value);
        else if (value is int)
            PlayerPrefs.SetInt(savingName, (int)value);
        else
            PlayerPrefs.SetString(savingName, (string)value);
    }

    public static int GetInt(string savingPref) => PlayerPrefs.GetInt(savingPref);
    public static float GetFloat(string savingPref) => PlayerPrefs.GetFloat(savingPref);
    public static string GetString(string savingPref) => PlayerPrefs.GetString(savingPref);
    public static int GetInt(string savingPref,int defaultValue) => PlayerPrefs.GetInt(savingPref, defaultValue);
    public static float GetFloat(string savingPref,float defaultValue) => PlayerPrefs.GetFloat(savingPref, defaultValue);
    public static string GetString(string savingPref,string defaultValue) => PlayerPrefs.GetString(savingPref, defaultValue);

}