using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName ="ManagerUpdate",menuName = "ScriptableObjects/ManagerUpdate")]
public class ManagerUpdate: ScriptableObject
{
    public bool IsUpdate = true;
 /*   public bool IsFixedUpdate = true;
    public bool IsLateUpdate = true;*/
    
    private static event Action onUpdate;
    /*
    private static event Action onFixedUpdate;
    private static event Action onLateUpdate;
    */
    public void Update()
    {
        if (!IsUpdate) return;
        onUpdate?.Invoke();
    }
    /*
    public void FixedUpdate()
    {
        if (!IsFixedUpdate) return;
        onFixedUpdate?.Invoke();
    }
    public void LateUpdate()
    {
        if (!IsLateUpdate) return;
        onLateUpdate?.Invoke();
    }
    */
    public static void AddUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onUpdate += item;
        }
    }
    
    public static void RemoveUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onUpdate -= item;
        }
    }
    /*
    public static void AddFixedUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onFixedUpdate += item;
        }
    }

    public static void RemoveFixedUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onFixedUpdate -= item;
        }
    }

    public static void AddLateUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onLateUpdate += item;
        }
    }

    public static void RemoveLateUpdate(params Action[] action)
    {
        foreach (var item in action)
        {
            onLateUpdate -= item;
        }
    }
    */
    public void OnDisable()
    {
        Clear();
    }
    public static void Clear()
    {
        onUpdate = null;
        /*
        onFixedUpdate = null;
        onLateUpdate = null;*/
    }
    /*
    public static void ClearUpdate()
    {
        onUpdate = null;
    }
    public static void ClearFixedUpdate()
    {
        onFixedUpdate = null;
    }
    public static void ClearLateUpdate()
    {
        onLateUpdate = null;
    }*/
}