using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Toolboxes;
public class PoolSystem : Singletoner<PoolSystem>
{
	private static Dictionary<string, Product> s_poolDictionary = new Dictionary<string, Product>();
	private LevelTasks _levelTasks;
    private void OnEnable()
    {
		EntitySystem.onLevelInit += OnLevelInit;
	}
    private void OnDisable()
    {
		EntitySystem.onLevelInit -= OnLevelInit;
	}
	public void OnLevelInit()
	{
		_levelTasks = EntitySystem.s_LevelTasks;
		s_poolDictionary.Clear();
		foreach (var product in _levelTasks.ProductsInLevel) {
			s_poolDictionary.Add(product.Name, product);
		}
	}
	public static Product GetFromPool(string tag, Vector3 position, Quaternion rotation,Transform parent)
	{
		Product productTemp = Instantiate(s_poolDictionary[tag], position, rotation, parent);
		productTemp.Starter();
		return productTemp;
	}
	public static void PutToPull(Product product)
	{
		Destroy(product.gameObject);
	}
}