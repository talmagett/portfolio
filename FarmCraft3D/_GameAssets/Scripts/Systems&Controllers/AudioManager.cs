using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] musicsPack=new AudioClip[] { };
    private int currentSongIndex=0;
    private static AudioSource _audioSource;
    private static bool s_isVibration=true;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        _audioSource = GetComponent<AudioSource>();
    }
    private void OnEnable()
    {
        EntitySystem.onGameLoaded += OnGameLoaded;
    } 
    private void OnDisable()
    {
        EntitySystem.onGameLoaded -= OnGameLoaded;
    }
    void OnGameLoaded() {

        _audioSource.clip = musicsPack[0];
        _audioSource.Play();

        Vibration.Init();

        ManagerUpdate.AddUpdate(MusicController);
        bool isAudio = SaveSystem.GetInt("isAudio",1) == 1;
        SetAudioTurn(isAudio);
        s_isVibration = SaveSystem.GetInt("isVibration", 1) == 1;
        SetVibrationTurn(s_isVibration);
    }
    void MusicController() {
        if (GetComponent<AudioSource>().isPlaying == false)
        {
            currentSongIndex++;
            if (currentSongIndex >= musicsPack.Length)
            {
                currentSongIndex = 0;
            }
            _audioSource.clip = musicsPack[currentSongIndex];
            _audioSource.Play();
        }
    }
    public static void PlaySound(AudioClip audioClip) {
        _audioSource.PlayOneShot(audioClip);
    }
    public static void Vibrate(long milliseconds) {
        if(s_isVibration)
            Vibration.Vibrate(milliseconds);
    }
    public void SetAudioTurn(bool value)
    {
        SaveSystem.Save("isAudio", value ? 1 : 0);
        _audioSource.volume = value ? 1 : 0;
    }
    public void SetVibrationTurn(bool value)
    {
        s_isVibration = value;
        SaveSystem.Save("isVibration", value ? 1 : 0);
    }
}