using System;
using System.Collections;
using System.Collections.Generic;
using Toolboxes;
using UnityEngine;
public class EntitySystem : Singletoner<EntitySystem>
{
    [SerializeField] internal Transform Logic;
    [SerializeField] internal Transform View;
    [SerializeField] private bool loadFromSaves;
    [SerializeField] private int Level;
    public LevelController levelController;
    internal static FarmerController farmerController;
    internal static UIManager uiManager;
    internal static Camera CameraMain;
    internal static LevelTasks s_LevelTasks => Singleton._levelTasks;
    internal static UIManager.UIOrderManager UIOrderManager;

    public static event Action onGameLoaded;
    public static event Action onLevelInit;
    public static event Action onMoneyChange;

    private ManagerUpdate _managerUpdate;
    private LevelTasks _levelTasks;

    private static int s_playerMoney;
    #region Monobehaviours
    private void Start()
    {
        CameraMain=FindObjectOfType<Camera>();
        farmerController = FindObjectOfType<FarmerController>();
        uiManager = FindObjectOfType<UIManager>();
        _managerUpdate = Resources.Load<ManagerUpdate>("ManagerUpdate");
        s_playerMoney = SaveSystem.GetInt("Money");
        OnGameLoaded();
    }
    public void OnGameLoaded() {
        UIOrderManager = uiManager.OrderManager;
        onMoneyChange?.Invoke();
        levelController.LoadLastScene();
        onGameLoaded?.Invoke();
    }
    public void OnLevelInit() {
        YsoCorp.GameUtils.YCManager.instance.OnGameStarted(levelController.CurrentLevelIndex);
        _levelTasks = levelController.GetCurrentLevelTast;
        onLevelInit?.Invoke();
    }
    private void Update()
    {
        _managerUpdate.Update();
    }
    #endregion

    public static int GetMoneyValue() => s_playerMoney;
    public void AddMoney(int value)
    {
        StartCoroutine(AddMoneyIterator(value));
    }
    public static void Pay(int value)
    {
        s_playerMoney -= value;
        onMoneyChange?.Invoke();
    }
    private IEnumerator AddMoneyIterator(int value)
    {
        for (int i = 0; i < value; i += (value / 10))
        {
            s_playerMoney += (value / 10);
            onMoneyChange?.Invoke();
            yield return new WaitForSeconds(0.01f);
        }
    }
    public static void SaveMoney()
    {
        SaveSystem.Save("Money", s_playerMoney);
    }
    public static void WinGame()
    {
        uiManager.WinGame();
    }
    private void OnDestroy()
    {
        _managerUpdate.OnDisable();
    }
    private void OnDisable()
    {
        _managerUpdate.OnDisable();
    }
    [System.Serializable]
    public class LevelController {
        [SerializeField] 
        private List<LevelData> _levels = new List<LevelData>();
        private int _currentLevelIndex = 0;
        private LevelData currentLevel=null;
        public int CurrentLevelIndex => _currentLevelIndex;
        public LevelTasks GetCurrentLevelTast => currentLevel.CurrentLevelTask;
        public void LoadLastScene() {
            _currentLevelIndex = Singleton.loadFromSaves ? SaveSystem.GetInt("LastLevel", 0) : Singleton.Level;
            LoadLevel();
            Singleton.OnLevelInit();
            _levels[_currentLevelIndex % (_levels.Count - 1)].Init();
        }
        public void LoadNextScene()
        {
            _currentLevelIndex++;
            LoadLevel();
            Singleton.OnLevelInit();
            _levels[_currentLevelIndex % (_levels.Count - 1)].Init();
        }
        private void LoadLevel() {
            if (currentLevel == null)
                currentLevel = Instantiate(_levels[_currentLevelIndex % (_levels.Count - 1)], Vector3.zero, Quaternion.identity);
            else {
                Destroy(currentLevel.gameObject);
                currentLevel = Instantiate(_levels[_currentLevelIndex% (_levels.Count - 1)], Vector3.zero, Quaternion.identity);
            }
            SaveSystem.Save("LastLevel", _currentLevelIndex);
        }
    }
}