using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class FarmerController : MonoBehaviour
{
    public AnimationController FarmerAnimatorController;
    [SerializeField] private Transform _bagTransform;
    private DOTweenAnimation _bagPunchAnimation;
    private static Dictionary<string, Container> _containers = new Dictionary<string, Container>();
    private FarmerMovement _farmerMovement;
    private Bag bag;
    private void OnEnable()
    {
        EntitySystem.onGameLoaded += OnGameLoaded;
        EntitySystem.onLevelInit += OnLevelInit;
    }
    private void OnDisable()
    {
        EntitySystem.onGameLoaded -= OnGameLoaded;
        EntitySystem.onLevelInit -= OnLevelInit;
    }
    public void OnGameLoaded() {
        //bag.Clear();
        _bagPunchAnimation = _bagTransform.GetComponent<DOTweenAnimation>();
        _farmerMovement = GetComponent<FarmerMovement>();
    }
    public void OnLevelInit()
    {
        _containers.Clear();
        transform.position = Vector3.up * 0.3f;
        List<Product> productsInLevel = EntitySystem.s_LevelTasks.ProductsInLevel;
        for (int i = 1; i < _bagTransform.childCount; i++)
        {
           Destroy(_bagTransform.GetChild(i).gameObject);
        }
        List<Container> ProductsInLevel = new List<Container>();

        foreach (var product in productsInLevel)
        {
            ProductsInLevel.Add(new Container(product));
        }
        foreach (var product in ProductsInLevel)
        {
            _containers.Add(product.ItemName, product);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Product"))
        {
            if (other.TryGetComponent(out Product pickedProduct))
            {
                _containers[pickedProduct.Name].AddProduct(pickedProduct);
                pickedProduct.PickupFarmer(_bagTransform);
                //bag.AddProduct(other.transform);
                _bagPunchAnimation.DORestart();
            }
        }
        else if (other.CompareTag("Water")) {
            _farmerMovement.ResetPos();
        }
    }
    public static bool HasPlayerItem(string requiringProductName)
    {
        bool hasItem= _containers[requiringProductName].ItemsCount > 0;
        //EntitySystem.farmerController.bag.Sort();
        return hasItem;
    }
    public static Product GetProduct(string requiringProductName)
    {
        Product product= _containers[requiringProductName].GetProduct();
        //EntitySystem.farmerController.bag.Remove(product.transform);
        return product;
    }
    public void DisableMovement()
    {
        _farmerMovement.DisableMovement();
    }
    [System.Serializable]
    public class AnimationController
    {
        [SerializeField] private Animator _animator;
        public void Run(bool value)
        {
            _animator.SetBool("IsRunning", value);
        }
        public void Harvest()
        {
            _animator.SetTrigger("Harvest");
        }
    }
    //------------------------------
    [System.Serializable]
    public class Container
    {
        public Container(Product product)
        {
            RequiringItem = product;
        }
        public Product RequiringItem;
        public string ItemName => RequiringItem.Name;
        private Stack<Product> _itemsInBag = new Stack<Product>();
        public int ItemsCount => _itemsInBag.Count;
        public void AddProduct(Product product)
        {
            _itemsInBag.Push(product);
        }
        public Product GetProduct()
        {
            Product itemTemp = _itemsInBag.Pop();
            return itemTemp;
        }
    }
    [System.Serializable]
    public class Bag {
        private List<Transform> productsInBag = new List<Transform>();
        public void Clear() {
            print(productsInBag.Count);
            productsInBag.Clear();
            print(productsInBag.Count);
        }
        public void AddProduct(Transform productTransform) {
            productsInBag.Add(productTransform);
            productTransform.transform.DOLocalJump(Vector3.up*0.2f*productsInBag.Count, 2, 1, 0.5f);
        }
        public void Sort() {
            for (int i = 0; i < productsInBag.Count; i++)
            {
                productsInBag[i].transform.DOLocalMove(Vector3.up * 0.2f * i, 0.1f);
            }
        }
        public void Remove(Transform productTransform) {
            productsInBag.Remove(productTransform);
        }
    }
}