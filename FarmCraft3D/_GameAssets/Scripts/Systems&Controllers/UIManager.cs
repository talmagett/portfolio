using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
public class UIManager : MonoBehaviour
{
    [Header("Orders")]
    [SerializeField] private UIOrderManager _orderManager;
    [Space]
    [Header("Money")]
    [SerializeField] private TMP_Text _moneyText;
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _fpsText;
    [Space]
    [Header("GameStates")]
    [SerializeField] private GameObject _winPanel;
    private float _deltaTime;
    public UIOrderManager OrderManager
    {
        get
        {
            return _orderManager;
        }
    }
    [SerializeField] private AudioVibrationManager audioVibrationManager;
    #region Mono
    private void OnEnable()
    {
        EntitySystem.onMoneyChange += OnMoneyChange;
        EntitySystem.onGameLoaded += OnGameLoaded;
    }
    private void OnDisable()
    {
        EntitySystem.onMoneyChange -= OnMoneyChange;
        EntitySystem.onGameLoaded -= OnGameLoaded;
    }
    public void OnGameLoaded()
    {
        ManagerUpdate.AddUpdate(FPSShow);
        audioVibrationManager.Init();
    }
    public void FPSShow()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        float fps = 1f / _deltaTime;
        _fpsText.text = "fps:" + (int)fps;
    }
    #endregion
    public void PauseResumeGame()
    {
        Time.timeScale = Time.timeScale == 0 ? 1f : 0;
    }
    public void WinGame()
    {
        _winPanel.SetActive(true);
        YsoCorp.GameUtils.YCManager.instance.OnGameFinished(true);
        int sumReward =0;
        foreach (var item in EntitySystem.s_LevelTasks.Orders)
        {
            sumReward += item.GoldReward;
        }
        _scoreText.text = sumReward.ToString();
        DOTween.Restart("10");
        EntitySystem.farmerController.DisableMovement();
    }
    public void BtnNextLevel()
    {
        EntitySystem.SaveMoney();
        _orderManager.ClearOrders();
        _winPanel.SetActive(false);
        EntitySystem.Singleton.levelController.LoadNextScene();
    }
    private void OnMoneyChange()
    {
        _moneyText.text = EntitySystem.GetMoneyValue().ToString();
        _moneyText.transform.DORewind();
        _moneyText.transform.DOPunchScale(new Vector3(0.5f, 0.5f, 0.5f), 0.1f);
    }

    //classes
    [System.Serializable]
    public class UIOrderManager
    {
        [SerializeField] private RectTransform _ordersPanel;
        [SerializeField] private UIOrder _orderPanelPrefab;
        private Dictionary<string, UIOrder> _ordersData = new Dictionary<string, UIOrder>();
        public void AddOrderToList(Order order)
        {
            string productName = order.OrderingProduct.Name;
            UIOrder uiOrder = Instantiate(_orderPanelPrefab, _ordersPanel);
            uiOrder.SetData(productName, order.OrderingProduct.Icon, order.Number);
            _ordersData.Add(productName, uiOrder);
            Vector2 panelSize = _ordersPanel.sizeDelta;
            panelSize.y += 65;
            _ordersPanel.sizeDelta = panelSize;
        }
        public void ChangeNumber(string productName, int number)
        {
            _ordersData[productName].ChangeNumber(number);
        }
        public void ClearOrders() {
            UIOrder[] orders = _ordersPanel.GetComponentsInChildren<UIOrder>();
            for (int i = 0; i < orders.Length; i++)
            {
                Vector2 panelSize = _ordersPanel.sizeDelta;
                panelSize.y -= 65;
                _ordersPanel.sizeDelta = panelSize;
                Destroy(orders[i].gameObject);
            }
            _ordersData.Clear();
        }
    }

    [System.Serializable]
    public class AudioVibrationManager
    {
        [SerializeField] private GameObject _audio_On;
        [SerializeField] private GameObject _audio_Off;
        [SerializeField] private GameObject _vibration_On;
        [SerializeField] private GameObject _vibration_Off;
        public void Init() {
            _audio_On.SetActive(SaveSystem.GetInt("isAudio")==0);
            _audio_Off.SetActive(SaveSystem.GetInt("isAudio") == 1);
            _vibration_On.SetActive(SaveSystem.GetInt("isVibration") == 0);
            _vibration_Off.SetActive(SaveSystem.GetInt("isVibration") == 1);
        }
    }
}