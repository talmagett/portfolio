using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class FarmerMovement : MonoBehaviour
{
    [SerializeField] private AudioClip _waterFallSound;
    [Header("Movement")]
    private Camera _camera;
    [SerializeField] private float _movementSpeed;
    private CharacterController _characterController;
    private VariableJoystick _joystick;
    private Vector3 _forward, _right;
    private bool _isMoving;
    private void OnEnable()
    {
        EntitySystem.onGameLoaded += OnGameLoaded;
        EntitySystem.onLevelInit += OnLeveInit;
    }
    private void OnDisable()
    {
        EntitySystem.onGameLoaded -= OnGameLoaded;
        EntitySystem.onLevelInit -= OnLeveInit;
    }
    public void OnGameLoaded()
    {
        _camera = EntitySystem.CameraMain;
        _characterController = GetComponent<CharacterController>();
        this._joystick = FindObjectOfType<VariableJoystick>();
        _forward = _camera.transform.forward;
        _forward.y = 0;
        _forward = Vector3.Normalize(_forward); ;
        _right = Quaternion.Euler(new Vector3(0, 90, 0)) * _forward;
    }
    public void OnLeveInit() { 
        ManagerUpdate.AddUpdate(Movement);
    }
    public void Movement()
    {
        if (_joystick.Direction != Vector2.zero)
            Move();
        else
        {
            if (_isMoving)
            {
                _isMoving = false;
                EntitySystem.farmerController.FarmerAnimatorController.Run(_isMoving);
            }
        }
        if (!_characterController.isGrounded) { 
        _characterController.Move(Vector3.down * Time.deltaTime * 4); 
    }}
    public void ResetPos()
    {
        gameObject.SetActive(false);
        transform.localPosition = Vector3.up * 0.3f;
        gameObject.SetActive(true);
        AudioManager.PlaySound(_waterFallSound);
    }
    private void Move()
    {
        if (!_isMoving)
        {
            _isMoving = true;
            EntitySystem.farmerController.FarmerAnimatorController.Run(_isMoving);
        }
        Vector3 direction = new Vector3(_joystick.Horizontal, 0, _joystick.Vertical);
        Vector3 rightMovement = _right * _movementSpeed * Time.deltaTime * direction.x;
        Vector3 upMovement = _forward * _movementSpeed * Time.deltaTime * direction.z;
        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);
        transform.forward = heading;

        _characterController.Move(heading * _movementSpeed * Time.deltaTime);
    }
    public void DisableMovement() {
        EntitySystem.farmerController.FarmerAnimatorController.Run(false);
        ManagerUpdate.RemoveUpdate(Movement);
    }
}