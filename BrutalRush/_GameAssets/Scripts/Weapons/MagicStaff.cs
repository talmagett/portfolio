using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class MagicStaff : Weapon
{
    [Space]
    [Header("Data")]
    [SerializeField] private Transform _shootingPos;
    [SerializeField] private LayerMask _layerMask;
    private Color staffBaseColor = Color.white * 4;
    [Header("Basic")]
    [SerializeField] private GameObject _fireBall;
    [SerializeField] private float _fireExplosionArea;
    [SerializeField] private AudioClip _fireBallSound;
    [Space]
    [Header("Ult")]
    [SerializeField] private ParticleSystem _laserStart;
    [SerializeField] private ParticleSystem _laserEnd;
    [SerializeField] private Transform _ultEdge;
    [SerializeField] private float _ultWidth;
    [SerializeField] private LineRenderer _laserLineRenderer;
    [SerializeField] private AudioClip _laserSound;
    private Vector3 _targetPosition;
    private float _time = 0;
    public override void Init()
    {
        base.Init();
        _laserCollider = GetComponent<BoxCollider>();
        Vector3 laserSize = _laserCollider.size;
        laserSize.x = _ultWidth;
        _laserCollider.size= laserSize;
    }
    public override void Attack(float charge)
    {
        if (!canAttack) return;
        base.Attack(charge);
        StartCoroutine(Fireball());
    }
    private IEnumerator Fireball()
    {
        EntitySystem.playerController.animationController.PlayAnimation("Attack");
        yield return new WaitForSeconds(0.1f);
        PlayerController.isLock = false;
        yield return new WaitForSeconds(0.3f);
        try
        {
            _targetPosition = EntitySystem.playerController.GetNearestEnemy.transform.position;
            Transform fireBall = Instantiate(_fireBall, _shootingPos.position, Quaternion.identity).transform;
            AudioManager.PlaySound(_fireBallSound);
            _time = 0;
            fireBall.DOMove(_targetPosition, 0.7f).OnComplete(() => FireBallExplosion(fireBall));
        }
        catch {
            if (_targetPosition == null) { 
            }
        }
    }

    private void FireBallExplosion(Transform fireball)
    {
        ParticleSystem fireExplosion = fireball.GetChild(0).GetComponent<ParticleSystem>();
        fireExplosion.transform.SetParent(null);
        print(fireExplosion.gameObject);
        fireExplosion.Play();
        Destroy(fireExplosion.gameObject, 1.5f);
        Destroy(fireball.gameObject, 0.1f);
        List<Collider> hittedColliders = Physics.OverlapSphere(_targetPosition, _fireExplosionArea, _layerMask).ToList();
        foreach (var hit in hittedColliders)
        {
            if (hit.TryGetComponent(out Enemy enemy))
            {
                enemy.TakeDamage(Damage,4);
            }
        }
    }
    public override void Ultimate()
    {
        if (!canUltimate) return;
        base.Ultimate();
        StartCoroutine(LaserBeam());
    }
    private BoxCollider _laserCollider;
    private IEnumerator LaserBeam()
    {
        EntitySystem.playerController.animationController.PlayAnimation("Ultimate");
        yield return new WaitForSeconds(0.1f);
        _laserStart.Play();
        yield return new WaitForSeconds(0.5f);
        EntitySystem.TimeFreeze(0.2f, 0.6f);
        EntitySystem.ShakeCamera(1f, 0.2f, 10, 90);
        AudioManager.PlaySound(_laserSound);
        AudioManager.Vibrate(100);
        _laserEnd.Play();
        _laserLineRenderer.enabled = true;
        _laserLineRenderer.widthMultiplier = _ultWidth/4;
        Vector3 halfDist = (_ultEdge.position - transform.position) / 2;
        Vector3 laserEdge = Vector3.zero;
        while (laserEdge.z<10) {
            laserEdge += Vector3.forward * Time.deltaTime*100;
            _laserLineRenderer.SetPosition(1, laserEdge);
            if (_laserLineRenderer.widthMultiplier < _ultWidth)
                _laserLineRenderer.widthMultiplier += Time.deltaTime*10;
            yield return null;
        }
        _laserLineRenderer.SetPosition(1, laserEdge);
        _laserCollider.enabled = true;
        AudioManager.PlaySound(_laserSound);
        yield return new WaitForSeconds(0.3f);
        AudioManager.PlaySound(_laserSound);
        _laserCollider.enabled = false;
        _laserEnd.Stop();
        yield return new WaitForSeconds(0.3f);
        _laserLineRenderer.enabled = false;
        PlayerController.isLock = false;
    }
    public override void OnBossFightTap()
    {
        base.OnBossFightTap();
    }
    public override void AttackBoss()
    {
        base.AttackBoss();
        StartCoroutine(MegaFireBall());
    }

    IEnumerator MegaFireBall() {
        EntitySystem.playerController.animationController.PlayAnimation("Ultimate");
        yield return new WaitForSeconds(0.3f);
        Transform fireBall = Instantiate(_fireBall, _shootingPos.position, Quaternion.identity).transform;
        AudioManager.PlaySound(_fireBallSound);
        float currentScale = fireBall.localScale.x;
        fireBall.DOScale(currentScale*2, 0.1f);
        fireBall.DOMove(EntitySystem.levelBoss.transform.position, 0.1f).OnComplete(() =>
        {
            fireBall.DOScale(Vector3.zero, 0.2f);
            Destroy(fireBall.gameObject, 1);
            fireBall.GetChild(0).GetComponent<ParticleSystem>().Play();
        });

        //_weaponMeshRenderer.sharedMaterial.SetColor("_Color", staffBaseColor);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy)) {

            enemy.TakeDamage(Damage * 2,20);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _fireExplosionArea);

        Gizmos.color = Color.blue;
        Vector3 halfDist = (_ultEdge.position - transform.position) / 2;
        Gizmos.DrawWireCube(transform.position + halfDist, new Vector3(_ultWidth, 2, halfDist.z * 2));
    }
}