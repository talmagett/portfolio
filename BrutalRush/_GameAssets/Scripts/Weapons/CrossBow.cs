using DG.Tweening;
using System.Collections;
using UnityEngine;
public class CrossBow : Weapon
{
    [Space]
    [Header("Data")]
    [SerializeField] private Transform _shootingPos;
    [SerializeField] private Bolt _bolt;
    [SerializeField] private float _distance;
    [SerializeField] private AudioClip[] sounds = new AudioClip[] { };
    private Color bowBaseColor = Color.white * 4;
    [Header("Base")]
    [SerializeField] private float _angle;
    [SerializeField] private int _count;
    [Header("Ult")]
    [SerializeField] private float _ultAngle;
    [SerializeField] private int _ultCount;
    
    public override void Attack(float charge)
    {
        if (!canAttack) return;
        base.Attack(charge);
        AudioManager.PlaySound(sounds[Random.Range(0, sounds.Length)]);
        StartCoroutine(ShootBase());
        
    }
    private IEnumerator ShootBase()
    {
        float rotationDeltaToNearestEnemy=0;
        try
        {
            Vector3 nearesEnemyPos = EntitySystem.playerController.GetNearestEnemy.transform.position;
            Vector3 dir = nearesEnemyPos - transform.position;
            dir.y = 0;
            rotationDeltaToNearestEnemy = Quaternion.LookRotation(dir).eulerAngles.y;
        }
        catch {
            rotationDeltaToNearestEnemy = transform.eulerAngles.y;
        }
        EntitySystem.playerController.animationController.PlayAnimation("Attack");
        EntitySystem.playerController.Recoil(1, transform.eulerAngles);
        yield return new WaitForSeconds(0.1f);
        PlayerController.isLock = false;
        float anglePerShot = _angle / _count;
        for (int i = -(_count / 2); i <= _count / 2; i++)
        {
            ShootBolt(new Vector3(transform.eulerAngles.x, rotationDeltaToNearestEnemy/*+transform.eulerAngles.y*/ + anglePerShot * i, transform.eulerAngles.z));
        }
    }
    public override void Ultimate()
    {
        if (!canUltimate) return;
        base.Ultimate();
        AudioManager.PlaySound(sounds[Random.Range(0, sounds.Length)]);
        StartCoroutine(ShootUlt());
    }
    private IEnumerator ShootUlt()
    {
        EntitySystem.playerController.animationController.PlayAnimation("Ultimate");
        yield return new WaitForSeconds(0.1f);
        EntitySystem.TimeFreeze(0.2f, 0.6f);
        EntitySystem.ShakeCamera(0.7f, 0.4f, 10, 90);
        AudioManager.Vibrate(100);
        EntitySystem.playerController.Recoil(10, transform.eulerAngles);
        float anglePerShot = _ultAngle / _ultCount;
        for (int i = -(_ultCount / 2); i <= _ultCount / 2; i++)
        {
            Vector3 angles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + anglePerShot * i, transform.eulerAngles.z);
            Bolt bolt = Instantiate(_bolt, _shootingPos.position, Quaternion.Euler(angles));
            bolt.SetBolt(_distance * 2, Damage);
            bolt.transform.localScale *= 2;
        }
        yield return new WaitForSeconds(0.1f);
        PlayerController.isLock = false;
    }
    public override void OnBossFightTap()
    {
        base.OnBossFightTap();
    }
    public override void AttackBoss()
    {
        base.AttackBoss();
        AudioManager.PlaySound(sounds[Random.Range(0, sounds.Length)]);
        AudioManager.PlaySound(sounds[Random.Range(0, sounds.Length)]);
        EntitySystem.playerController.animationController.PlayAnimation("Attack");
        EntitySystem.playerController.Recoil(5, transform.eulerAngles);
        //_weaponMeshRenderer.sharedMaterial.SetColor("_Color", bowBaseColor);
        for (int i = 0; i < 24; i++)
        {
            Instantiate(_bolt, _shootingPos.position, Quaternion.Euler(transform.eulerAngles.x + Random.Range(-10, 10), transform.eulerAngles.y + Random.Range(-10, 10), transform.eulerAngles.z + Random.Range(-10, 10))).SetBolt(_distance * 4, Damage);
        }
    }
    private void ShootBolt(Vector3 angles)
    {
        Instantiate(_bolt, _shootingPos.position, Quaternion.Euler(angles)).SetBolt(_distance * 2, Damage);
    }
}