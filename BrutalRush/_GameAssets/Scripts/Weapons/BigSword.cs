using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class BigSword : Weapon
{
    [Space]
    [Header("Data")]
    [SerializeField] private LayerMask _layerMask;
    private Color swordBaseColor = Color.white * 4;
    [Header("Basic")]
    [SerializeField] private float _radius;
    [SerializeField] private ParticleSystem _slashEffect;
    [SerializeField] private AudioClip[] _slashSound=new AudioClip[] { };
    [Header("Ult")]
    [SerializeField] private float _ultRadius;
    [SerializeField] private ParticleSystem _slashUltEffect;
    [SerializeField] private AudioClip _bigSlashSound;
    public override void Attack(float charge)
    {
        if (!canAttack) return;
        base.Attack(charge);
        StartCoroutine(Slash());
    }
    private IEnumerator Slash()
    {
        EntitySystem.playerController.animationController.PlayAnimation("Attack");
        yield return new WaitForSeconds(0.1f);
        PlayerController.isLock = false;
        yield return new WaitForSeconds(0.2f);
        EntitySystem.TimeFreeze(0.1f, 0.5f);
        _slashEffect.Play();
        AudioManager.PlaySound(_slashSound[Random.Range(0, _slashSound.Length)]);
        List<Collider> hittedColliders = Physics.OverlapSphere(transform.position, _radius, _layerMask).ToList();
        Vector3 forward = transform.forward;
        hittedColliders = hittedColliders.Where(x => (
            Vector3.Dot(forward, (x.transform.position - transform.position).normalized) > -0.3f
        )).ToList();
        foreach (var hit in hittedColliders)
        {
            if (hit.TryGetComponent(out Enemy enemy))
            {
                enemy.TakeDamage(Damage,8);
            }
        }
    }
    public override void Ultimate()
    {
        if (!canUltimate) return;
        base.Ultimate();
        StartCoroutine(SlashFull());
    }

    private IEnumerator SlashFull()
    {
        EntitySystem.playerController.animationController.PlayAnimation("Ultimate");
        yield return new WaitForSeconds(0.2f);
        EntitySystem.TimeFreeze(0.2f,0.6f);
        EntitySystem.ShakeCamera(0.7f,0.4f,10,90);
        AudioManager.Vibrate(100);
        AudioManager.PlaySound(_bigSlashSound);
        _slashUltEffect.Play();
        List<Collider> hittedColliders = Physics.OverlapSphere(transform.position, _ultRadius, _layerMask).ToList();
        foreach (var hit in hittedColliders)
        {
            if (hit.TryGetComponent(out Enemy enemy))
            {
                enemy.TakeDamage(Damage*2,10);
            }
        }
        yield return new WaitForSeconds(0.1f);
        PlayerController.isLock = false;
    }
    public override void OnBossFightTap()
    {
        base.OnBossFightTap();
    }
    public override void AttackBoss()
    {
        base.AttackBoss();
        AudioManager.PlaySound(_bigSlashSound);
        EntitySystem.playerController.animationController.isSuperAttack=false;
        EntitySystem.playerController.animationController.PlayAnimation("Attack");
        _slashEffect.transform.localScale *= 4;
        var particle = _slashEffect.main;
        particle.simulationSpeed /= 2;
        _slashEffect.Play();
        //_weaponMeshRenderer.sharedMaterial.SetColor("_Color", swordBaseColor);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, _radius);
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, _ultRadius);
    }
}