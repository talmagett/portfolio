using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bolt : MonoBehaviour
{
    [SerializeField] private float _movingSpeed;
    private float _distance;
    private int _damage;
    private float multiplicator=2;
    private Vector3 basePos;
    public void SetBolt(float distance, int damage) {
        basePos = transform.position;
        _damage = damage;
        _distance = distance;
        ManagerUpdate.AddUpdate(MoveForward);
        ManagerUpdate.AddUpdate(Decrease);
    }
    private void Decrease()
    {
        if (multiplicator > 1) multiplicator -= Time.deltaTime*4;
        else {
            multiplicator = 1;
            ManagerUpdate.RemoveUpdate(Decrease);
        }
    }
    private void MoveForward() {
        transform.position+=transform.forward*Time.deltaTime* _movingSpeed*multiplicator;
        if (Vector3.Distance(basePos, transform.position) >= _distance) {
            ManagerUpdate.RemoveUpdate(MoveForward);
            GetComponent<Collider>().enabled = false;
            GetComponentInChildren<MeshRenderer>().enabled = false;
            Destroy(gameObject,1);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyCharacter"))
        {
            if (other.TryGetComponent(out Enemy enemy))
            {
                enemy.TakeDamage(_damage,1);
            }
        }
    }
}