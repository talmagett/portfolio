using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
public class PlayerController : Character
{
    [SerializeField] private PlayerStats stats;
    private int Health;
    internal int AttackDamage;
    internal float AttackSpeed;
    internal float UltCharge;

    private Weapon _equppedWeapon;
    [SerializeField] internal AnimationController animationController;
    [SerializeField] private SkinnedMeshRenderer _meshRenderer;
    private PlayerMovement _playerMovement;
    public Vector3 HeroPosition => transform.position;
    private PlayerFieldOfAttack _playerFieldOfAttack;
    //----------Weapons-----------
    [Space]
    [SerializeField] private Transform _weaponsInventory;
    [SerializeField] private ParticleSystem _ultCollect;
    [SerializeField] private ParticleSystem _ultReady;
    [SerializeField] private ParticleSystem _hitEffect;
    [SerializeField] private ParticleSystem _dieEffect; 
    private Weapon[] weapons;
    [Space]
    [SerializeField] private bool _canAttack;
    [SerializeField] private bool _canUltimate;
    #region Mono
    void OnEnable()
    {
        EntitySystem.Phases.onGameStateChange += GamePhaseChange;
    }
    void OnDisable()
    {
        EntitySystem.Phases.onGameStateChange -= GamePhaseChange;
    }
    private void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.init: 
                Init(); 
                break;
            case GameState.battle: 
                Battle(); 
                break;
            case GameState.bossDuel:
                BossDuel();
                break;
            case GameState.finish: break;
            case GameState.lose: Lose(); break;
        }
    }
    public override void Init()
    {
        _playerMovement = GetComponent<PlayerMovement>();
        _playerMovement.Init(_movementSpeed);
        _playerFieldOfAttack = GetComponentInChildren<PlayerFieldOfAttack>();
        _playerFieldOfAttack.Init();
        weapons = _weaponsInventory.GetComponentsInChildren<Weapon>(true);
        SetWeapon(weapons[SaveSystem.GetInt("LastChosenWeapon", 1)]);
        stats.LoadDataFromSaves();
    }
    private void Battle()
    {
        animationController.SetState(SaveSystem.GetInt("LastChosenWeapon", 1));
        _equppedWeapon = GetComponentInChildren<Weapon>();
        _equppedWeapon.Init();
        _playerFieldOfAttack.SetRange(_equppedWeapon.AttackRange);
        Health = (int)stats.health.CurrentValue;
        AttackDamage = (int)stats.attack.CurrentValue;
        AttackSpeed = stats.attackSpeed.CurrentValue;
        UltCharge = stats.ultCharge.CurrentValue;
        _maxHealth = Health;
        _health = _maxHealth;
        ManagerUpdate.AddUpdate(FightingUpdate);
    }
    private void BossDuel() {
        _ultCollect.Play();
        ManagerUpdate.RemoveUpdate(FightingUpdate, _playerMovement.MovementUpdate);
        animationController.isMoving = false;
        UIManager.onBossFightTap += _equppedWeapon.OnBossFightTap;
        transform.LookAt(EntitySystem.levelBoss.transform.position);
    }
    public void BossFightVictory()
    {
        UIManager.onBossFightTap -= _equppedWeapon.OnBossFightTap;
        _equppedWeapon.AttackBoss();
    }
    private void Lose() {
        ManagerUpdate.RemoveUpdate(FightingUpdate, _playerMovement.MovementUpdate);
        animationController.isMoving = false;
    }
    internal static bool isLock = false;
    private Vector3 velocity= Vector3.zero;
    public void FightingUpdate()
    {

        Vector3 targetPos = new Vector3(0, 12, transform.position.z + 14);
        EntitySystem.mainCamera.transform.position = Vector3.Lerp(EntitySystem.mainCamera.transform.position, targetPos, 0.1f);
        /*if (isEnemy && !isLock)
        {
            var lookDir = GetNearestEnemy.transform.position - transform.position;
            lookDir.y = 0;
            transform.rotation = Quaternion.LookRotation(lookDir);
        }*/
        if (_canAttack)
            if (isEnemy && _equppedWeapon.canAttack)
            {
                _equppedWeapon.Attack(AttackDamage);
                isLock = true;
            }
        if (Input.touchCount > 0) {
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                Ultimate();
            }
        }
        if (_equppedWeapon.canUltimate)
            _ultReady.Play();
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0)) {

            Ultimate();
        }
#endif
    }
    private void Ultimate()
    {
        if (_canUltimate) 
            if (_equppedWeapon.canUltimate&&!isLock)
            {
                _equppedWeapon.Ultimate();
                _ultReady.Stop();
                isLock = true;
            }
    }
    
    public override void TakeDamage(int dmgValue)
    {
        _health -= dmgValue;
        EntitySystem.uiManager.HealthInPercent = (float)_health / _maxHealth;
        _hitEffect.Play();
        if (_health <= 0) Die();
        else {
            if(isGetHit)
                animationController.PlayAnimation("GetHit");
        }
    }
    private bool isGetHit=false;
    public void Recoil(float power, Vector3 direction) {
        StartCoroutine(GetHit(power,direction));
    }
    private IEnumerator GetHit(float power, Vector3 direction)
    {
        isGetHit = true;
        Vector3 dir = (direction - transform.position).normalized;
        for (int i = 0; i < 10; i++)
        {
            transform.position -= dir * Time.deltaTime * power;
            yield return null;
        }
        isGetHit = false;
    }
    public override void Die()
    {
        var dieEffect = Instantiate(_dieEffect, transform.position, Quaternion.identity).main;
        dieEffect.startColor = _meshRenderer.material.color;
        _meshRenderer.enabled = false;
        EntitySystem.Phases.SetGameStage(GameState.lose);
    }
    float rateOverTime = 0;
    public void IncreaseUltPower() {
        rateOverTime++;
        var emission= _ultCollect.emission;
        emission.rateOverTimeMultiplier = rateOverTime;
    }
    public void StopUltCollect() {
        _ultCollect.Stop();
    }
    #endregion
    #region EnemiesList
    private List<Enemy> enemies = new List<Enemy>();
    internal Enemy GetNearestEnemy => enemies.OrderBy(t => Vector3.Distance(transform.position, t.transform.position)).FirstOrDefault();
    private bool isEnemy => enemies.Count > 0;
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
    public void RemoveEnemyFromList(Enemy enemy)
    {
        enemies.Remove(enemy);
    }
    #endregion

    public void SetWeapon(Weapon weapon)
    {
        _equppedWeapon = weapon;
        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i] == _equppedWeapon)
            {
                weapons[i].gameObject.SetActive(true);
                EntitySystem.uiManager.ChooseWeapon(i);
                SaveSystem.Save("LastChosenWeapon", i);
            }
            else weapons[i].gameObject.SetActive(false);
            weapons[i].Init();
        }
    }
    public void LevelUpStat(string attribute) {
        switch (attribute)
        {
            case "health":
                stats.health.LevelUp(attribute); 
                break;
            case "attack":
                stats.attack.LevelUp(attribute); 
                break;
            case "attackSpeed":
                stats.attackSpeed.LevelUp(attribute);
                break;
            case "ultCharge":
                stats.ultCharge.LevelUp(attribute);
                break;
        }
    }
    #region Classes
    [System.Serializable]
    public class PlayerStats
    {
        // ONLY NUMBERS
        [SerializeField] internal Stat<int> health;
        [SerializeField] internal Stat<int> attack;
        [SerializeField] internal Stat<float> attackSpeed;
        [SerializeField] internal Stat<float> ultCharge;
        public void LoadDataFromSaves() {
            health.LoadDataFromSaves("health");
            attack.LoadDataFromSaves("attack");
            attackSpeed.LoadDataFromSaves("attackSpeed");
            ultCharge.LoadDataFromSaves("ultCharge");
        }
        [System.Serializable]
        public class Stat<T>
        {
            [SerializeField] private T _baseValue;
            [SerializeField] private T _multiplicator;
            [Min(0)]
            private int _level = 0;
            [SerializeField] private int _baseCost;
            [SerializeField] private int _costMultiplicator;
            internal int Cost => _baseCost + (_costMultiplicator * _level);
            internal float CurrentValue =>
               (float)(Convert.ToDouble(_baseValue) + _level * Convert.ToDouble(_multiplicator));
            internal void LoadDataFromSaves(string attribute) {
                _level=SaveSystem.GetInt(attribute,0);
                EntitySystem.uiManager.HeroLevelCostChange(AttributeIndex(attribute), Cost, _level);
            }
            internal void LevelUp(string attribute)
            {
                if (UIManager.Money >= Cost)
                {
                    EntitySystem.uiManager.ChangeMoney(-Cost);
                    _level++; 
                    EntitySystem.uiManager.HeroLevelCostChange(AttributeIndex(attribute), Cost, _level);
                    SaveSystem.Save(attribute, _level);
                }
            }
            private int AttributeIndex(string name) {
                return name == "health" ? 0 : name == "attack" ? 1 : name == "attackSpeed" ? 2 : 3;
            }
        }
    }
    [System.Serializable]
    public class AnimationController
    {
        [SerializeField] private Animator _animator;
        internal void SetState(int stateIndex) {
            _animator.SetInteger("State", stateIndex+1);
        }
        internal bool isMoving { 
            set {
                _animator.SetBool("isMoving",value);
            } 
        }
        internal bool isSuperAttack {
            set { 
                _animator.SetBool("isSuperAttack",value);
            }
        }
        internal void PlayAnimation(string nameValue) {
            _animator.SetTrigger(nameValue);
        }
    }
    #endregion
}