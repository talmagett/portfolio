using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController characterController;
    private Camera cameraMain;
    private Vector3 _forward, _right;
    private bool _isMoving;
    float _movementSpeed;
    internal void Init(float _movementSpeed) {
        ManagerUpdate.AddUpdate(GravityUpdate, MovementUpdate);

        characterController = GetComponent<CharacterController>();
        this._movementSpeed = _movementSpeed;
        cameraMain = EntitySystem.mainCamera;

        _forward = cameraMain.transform.forward;
        _forward.y = 0;
        _forward = Vector3.Normalize(_forward); ;
        _right = Quaternion.Euler(new Vector3(0, 90, 0)) * _forward;
    }
    private void GravityUpdate() {
        if (!characterController.isGrounded)
            characterController.Move(Vector3.down * Time.deltaTime * 9.8f);
    }
    public void MovementUpdate() {
        if (EntitySystem.uiManager.JoystickDirection != Vector3.zero&&!PlayerController.isLock)
        {
            _isMoving = true;
            EntitySystem.playerController.animationController.isMoving = true;
            Vector3 direction = EntitySystem.uiManager.JoystickDirection;
            Vector3 rightMovement = _right * _movementSpeed * Time.deltaTime * direction.x;
            Vector3 upMovement = _forward * _movementSpeed * Time.deltaTime * direction.z;
            Vector3 heading = Vector3.Normalize(rightMovement + upMovement);
            transform.forward = heading;

            characterController.Move(heading * _movementSpeed * Time.deltaTime);
        }
        else
        {
            if (_isMoving)
            {
                _isMoving = false;
                EntitySystem.playerController.animationController.isMoving = false;
            }
        }
    }
}