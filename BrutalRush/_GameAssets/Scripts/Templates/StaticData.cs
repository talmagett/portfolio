using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticData : MonoBehaviour
{
    [Header("Distance")]
    [Space]
    [SerializeField] private Transform _startingPoint;
    [SerializeField] private Transform _finisgingPoint;
    [SerializeField] private Transform _finishMultiply;
    internal Transform Mulpiply => _finishMultiply;
    #region Mono
    void OnEnable()
    {
        EntitySystem.Phases.onGameStateChange += GamePhaseChange;
    }
    void OnDisable()
    {
        EntitySystem.Phases.onGameStateChange -= GamePhaseChange;
    }
    private void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.init:
                ManagerUpdate.AddUpdate(DistanceSlider);
                break;
            case GameState.battle:
                break;
            case GameState.bossDuel:
                break;
            case GameState.finish: 
                break;
            case GameState.lose: 
                break;
        }
    }
    #endregion
    private void DistanceSlider()
    {
        float heroZPos=EntitySystem.playerController.HeroPosition.z;
        float currentPoint = heroZPos - _startingPoint.position.z;
        float startingPoint = _finisgingPoint.position.z - _startingPoint.position.z;
        EntitySystem.uiManager.DistanceScaler = currentPoint / startingPoint;
    }
}