using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulseWall : MonoBehaviour
{
    [SerializeField] private float _mulptiply;
    [SerializeField] private Material _material;
    [SerializeField] private AudioClip _clip;
    private Rigidbody[] _shatters = new Rigidbody[] { };
    void Start()
    {
        _shatters = GetComponentsInChildren<Rigidbody>();
    }

    public void Impulse() {
        EntitySystem.uiManager.SetMultiply(_mulptiply);
        for (int i = 0; i < _shatters.Length; i++)
        {
            _shatters[i].isKinematic = false;
            Vector3 direction = (_shatters[i].transform.position - transform.position).normalized;
            _shatters[i].AddForce(-Vector3.forward *10+ direction * Random.Range(40, 80), ForceMode.Impulse);
        }
        AudioManager.PlaySound(_clip);
    }
    [ContextMenu("Color")]
    public void ColorMaterial() {
        MeshRenderer[] meshs = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < meshs.Length; i++)
        {
            meshs[i].material = _material;
        }
    }
}
