using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    [SerializeField] private TMP_Text _FPSText;
    [SerializeField] private TMP_Text _moneyText;
    [SerializeField] private TMP_Text _messageDebugger;
    [Space]
    [Header("Stats")]
    [SerializeField] private Transform _upgrades;
    private TMP_Text[] _levelTexts = new TMP_Text[4];
    private TMP_Text[] _costTexts = new TMP_Text[4];
    [Space]
    [SerializeField] private Slider _distanceSlider;
    [Space]
    [SerializeField] private Slider _ultSlider;
    [Space]
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private RectTransform[] weaponBtns = new RectTransform[] { };
    internal float DistanceScaler
    {
        set
        {
            _distanceSlider.value = value;
        }
    }
    internal float UltCharge {
        set {
            _ultSlider.value = value;
        }
    }
    internal float HealthInPercent
    {
        set { 
            _healthSlider.value = value;
        }
    }
    [Space]
    [Header("BossDuel")]
    [SerializeField] private GameObject _bossFightPanel;
    [SerializeField] private Slider _bossSlider;
    [Space]
    [Header("Finish")]
    [SerializeField] private GameObject _winWindowPanel;
    [SerializeField] private GameObject _loseWindowPanel;
    [SerializeField] private TMP_Text _rewardWinText;
    [SerializeField] private TMP_Text _rewardLostText;
    private float _deltaTime;
    private FloatingJoystick _joystick;
    internal Vector3 JoystickDirection => new Vector3(_joystick.Direction.x, 0, _joystick.Direction.y);
    public static event System.Action onBossFightTap;

    #region Money
    [Min(0)]
    private static int s_money = 1000;
    internal static int Money => s_money;
    internal void ChangeMoney(int value)
    {
        s_money += value;
        SaveSystem.Save("Money", s_money);
        _moneyText.text = s_money.ToString();
    }
    #endregion
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void ResetInit()
    {
        s_money = 0;
        onBossFightTap = null;
    }
    public static void DebugMessage(string value) {
        EntitySystem.uiManager._messageDebugger.text = value;
    }
    #region Mono
    void OnEnable()
    {
        EntitySystem.Phases.onGameStateChange += GamePhaseChange;
    }
    void OnDisable()
    {
        EntitySystem.Phases.onGameStateChange -= GamePhaseChange;
    }
    private void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.init:
                //Init();
                break;
            case GameState.battle:
                _distanceSlider.gameObject.SetActive(true);
                _healthSlider.gameObject.SetActive(true);
                _ultSlider.gameObject.SetActive(true);
                break;
            case GameState.bossDuel:
                BossFightPhase();
                break;
            case GameState.finish:
                WinWindow();
                break;
            case GameState.lose:
                LoseWindow();
                break;
        }
    }
    private void Awake()
    {
        ManagerUpdate.AddUpdate(FPSShow);
        _joystick = GetComponentInChildren<FloatingJoystick>();
        ChangeMoney(SaveSystem.GetInt("Money", s_money));
        for (int i = 0; i < _levelTexts.Length; i++)
        {
            _costTexts[i] = _upgrades.GetChild(i).GetChild(0).GetComponentInChildren<TMP_Text>();
            _levelTexts[i] = _upgrades.GetChild(i).GetChild(1).GetComponentInChildren<TMP_Text>();
        }
    }
    #endregion
    internal void ChooseWeapon(int chosenWeapon) {
        for (int i = 0; i < weaponBtns.Length; i++)
        {
            if (i == chosenWeapon) weaponBtns[i].localScale = Vector3.one * 1.2f;
            else weaponBtns[i].localScale = Vector3.one;
        }
    }
    public void HeroLevelCostChange(int index,int level,int cost)
    {
        _levelTexts[index].text = level.ToString();
        _costTexts[index].text = cost.ToString();
    }
    public void FPSShow()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        float fps = 1f / _deltaTime;
        _FPSText.text = "fps:" + (int)fps;
    }
    private float pushingPower=0;
    private float pushingCounter=1;
    private void Counter() {
        if(pushingCounter>0)
            pushingCounter -= Time.deltaTime;
    }
    public void AddCharge()
    {
        _bossSlider.value++;
        EntitySystem.ShakeCamera(0.1f, 0.4f, 10, 90);
        //EntitySystem.cinemachine.m_Lens.FieldOfView += 0.5f;
        pushingPower += pushingCounter;
        pushingCounter = Random.Range(0.1f,0.5f); ;
        onBossFightTap?.Invoke();
        if (_bossSlider.value >= _bossSlider.maxValue)
        {
            EntitySystem.playerController.BossFightVictory();
            EntitySystem.levelBoss.SetPushingPower(pushingPower);
            _bossFightPanel.SetActive(false);
        }
    }
    private float _multiply=1;
    public void SetMultiply(float mult) {
        _multiply = mult;
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void NextLevel(int index) {
        SceneManager.LoadScene(index);
    }
    public void BossFightPhase()
    {
        _distanceSlider.gameObject.SetActive(false);
        _healthSlider.gameObject.SetActive(false);
        _ultSlider.gameObject.SetActive(false);
        _joystick.gameObject.SetActive(false);
        _bossFightPanel.SetActive(true);
        ManagerUpdate.AddUpdate(Counter);
    }
    public void ChooseWeapon(Weapon weapon)
    {
        EntitySystem.playerController.SetWeapon(weapon);
    }
    public void WinWindow()
    {
        ManagerUpdate.RemoveUpdate(Counter);

        _winWindowPanel.SetActive(true);
        int money = (int)(Random.Range(80,100)* _multiply);
        ChangeMoney(money);
        _rewardWinText.text = money.ToString();
    }
    public void LoseWindow()
    {
        ManagerUpdate.RemoveUpdate(Counter);

        _loseWindowPanel.SetActive(true);
        int money = (int)(Random.Range(80, 100) * _distanceSlider.value);
        ChangeMoney(money);
        _rewardLostText.text = money.ToString();
    }
    public void PauseGame()
    {
        EntitySystem.Singleton.PauseGame();
    }
    //TODO: ���� ������ ������ ���� ��� ����������� ��� ��������� ��� ����� ����
}