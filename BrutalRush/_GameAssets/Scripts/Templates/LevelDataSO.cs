using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LevelData", menuName = "SO/LevelData", order = 52)]
public class LevelDataSO : ScriptableObject
{
    [SerializeField] internal List<Wave> waves=new List<Wave>();
    [SerializeField] internal Boss boss;
    [System.Serializable]
    public class Wave {
        [SerializeField] internal Enemy SpawningEnemy;
        [SerializeField] internal int count;
    }
}