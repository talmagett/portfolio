using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private LevelDataSO _levelDataSO;
    [SerializeField] private Transform _spawningTransform;
    [SerializeField] private float spawnTimer=4;
    [SerializeField] private ParticleSystem _spawningEffect;
    [SerializeField] private int _count;
    private float spawnCounter=0;
    private int waveNumber;
    private Transform[] spawingPoints;

    private void OnEnable()
    {
        EntitySystem.Phases.onGameStateChange += GamePhaseChange;
    }

    private void OnDisable()
    {
        EntitySystem.Phases.onGameStateChange -= GamePhaseChange;
    }

    private void Start()
    {
        Enemy[] enemies = _spawningTransform.GetComponentsInChildren<Enemy>(true);
        for (int i = 0; i < enemies.Length; i++)
        {
            PoolSystem.AddToPool(enemies[i].Name,enemies[i]);
        }
    }

    private void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.battle:
                {
                    spawingPoints = new Transform[transform.childCount];
                    for (int i = 0; i < spawingPoints.Length; i++)
                    {
                        spawingPoints[i] = transform.GetChild(i);
                    }
                    ManagerUpdate.AddUpdate(Spawn);
                }
                break;
            case GameState.bossDuel:
                ManagerUpdate.RemoveUpdate(Spawn);
                break;
        }
    }

    private void Spawn() {
        spawnCounter -= Time.deltaTime;
        if (spawnCounter <= 0)
        {
            spawnCounter = spawnTimer;
            for (int i = 0; i < _levelDataSO.waves[waveNumber].count; i++)
            {
                int spawningGateIndex = Random.Range(0, spawingPoints.Length);
                Instantiate(_spawningEffect, spawingPoints[spawningGateIndex].transform.position,Quaternion.identity);
                 Enemy enemy= PoolSystem.GetFromPool(_levelDataSO.waves[waveNumber].SpawningEnemy.name);
                if (enemy != null) {
                    enemy.transform.SetPositionAndRotation(spawingPoints[spawningGateIndex].transform.position, Quaternion.identity);
                    enemy.gameObject.SetActive(true);
                    enemy.Spawned();
                }
                else {
                    Enemy enemy1=Instantiate(_levelDataSO.waves[waveNumber].SpawningEnemy, _spawningTransform);
                    enemy1.transform.SetPositionAndRotation(spawingPoints[spawningGateIndex].transform.position, Quaternion.identity);
                    enemy1.Spawned();
                }
            }
            if (waveNumber < _levelDataSO.waves.Count-1)
                waveNumber++;
            else
                waveNumber=0;
        }
    }

    [ContextMenu("Recreate")]
    public void RecreateObjects() {
        while (_spawningTransform.childCount>0) {
            DestroyImmediate(_spawningTransform.GetChild(0).gameObject);
        }
        InstantiateObjects();
    }
    private void InstantiateObjects() {

        for (int i = 0; i < _levelDataSO.waves.Count; i++)
        {
            for (int j = 0; j < _count * _levelDataSO.waves[i].count; j++)
            {
                Instantiate(_levelDataSO.waves[i].SpawningEnemy, _spawningTransform).gameObject.SetActive(false);
            }
        }
    }

}
public class PoolSystem {

    public class EnemiesList {
        private Queue<Enemy> enemiesList = new Queue<Enemy>();
        public void AddToList(Enemy enemy) {
            enemiesList.Enqueue(enemy);
        }
        public Enemy GetFromList() {
            if (enemiesList.Count > 0)
                return enemiesList.Dequeue();
            else return null;
        }
    }

    private static Dictionary<string, EnemiesList> poolOfEnemies = new Dictionary<string, EnemiesList>();
    public static void AddToPool(string type, Enemy enemy)
    {
        if (poolOfEnemies.TryGetValue(type, out EnemiesList list)) {
            list.AddToList(enemy);
        }
        else
        {
            poolOfEnemies.Add(type, new EnemiesList());
            poolOfEnemies[type].AddToList(enemy);
        }
    }
    public static Enemy GetFromPool(string type) {
        poolOfEnemies.TryGetValue(type,out EnemiesList value);
        return value.GetFromList();
    }
}