using UnityEngine;

public class PlayerFieldOfAttack : MonoBehaviour
{
    private PlayerController _playerTest;

    public void Init()
    {
        _playerTest = GetComponentInParent<PlayerController>();
    }
    public void SetRange(float attackRange) {
        transform.localScale = Vector3.one * attackRange;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            _playerTest.AddEnemyToList(enemy);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            _playerTest.RemoveEnemyFromList(enemy);
        }
    }
}