using UnityEngine;
public abstract class Weapon : MonoBehaviour, IAttackable, IUltable
{
    [SerializeField] private string _name;
    public string Name => _name;
    [SerializeField] protected WeaponSO weaponData;
    [SerializeField] protected GameObject weaponObject;
    [SerializeField] protected MeshRenderer _weaponMeshRenderer;
    private float _ultCharge = 0;
    public int Damage => (int)(weaponData.DamageValue*EntitySystem.playerController.AttackDamage);
    public float AttackRange=>weaponData.AttackRange;
    public bool canUltimate => _ultCharge >= weaponData.UltMaxCharge;
    public bool canAttack { get; private set; } = true;
    private Timer timer = null;
    public void SetData()
    {
        weaponObject.SetActive(gameObject.activeSelf);
    }
    public virtual void Init()
    {
        SetData();
    }
    public virtual void Attack(float charge)
    {
        AddCharge(charge);
        canAttack = false;
        Timer();
    }
    public virtual void Ultimate()
    {
        ResetCharges();
        canAttack = false;
        Timer();
    }
    public virtual void OnBossFightTap() {
        EntitySystem.playerController.animationController.isSuperAttack = true;
        EntitySystem.playerController.IncreaseUltPower();
    }
    public virtual void AttackBoss() {
        EntitySystem.playerController.StopUltCollect();
    }
    private void Timer()
    {
        if (timer == null)
        {
            timer = new Timer(() => canAttack = true, weaponData.AttackSpeedDelay*EntitySystem.playerController.AttackSpeed);
        }
        else { 
            timer.Restart();
        }
    }
    public void AddCharge(float charge)
    {
        _ultCharge += charge;
        EntitySystem.uiManager.UltCharge = _ultCharge;
    }
    public void ResetCharges()
    {
        _ultCharge = 0;
        EntitySystem.uiManager.UltCharge = _ultCharge;
    }
}