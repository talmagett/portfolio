using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour, IHitable
{
    [SerializeField] protected float _movementSpeed;
    [SerializeField] protected int _maxHealth;
    protected int _health;
    protected bool isDead;
    public abstract void Init();
    public virtual void TakeDamage(int dmgValue) {
    
    }
    public virtual void TakeDamage(int dmgValue, float power) {
    
    }
    public abstract void Die();
}