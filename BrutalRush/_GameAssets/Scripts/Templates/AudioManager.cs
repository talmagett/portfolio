using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private GameObject _AudioOnObj;
    [SerializeField] private GameObject _AudioOffObj;
    [SerializeField] private GameObject _VibrationOnObj;
    [SerializeField] private GameObject _VibrationOffObj;
    private static AudioSource audioSource;
    private static bool hasAudio = true;
    private static bool hasVibration = true;

    void OnEnable()
    {
        EntitySystem.Phases.onGameStateChange += GamePhaseChange;
    }
    void OnDisable()
    {
        EntitySystem.Phases.onGameStateChange -= GamePhaseChange;
    }
    private void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.init:
                Init();
                break;
            case GameState.battle:
                break;
            case GameState.bossDuel:
                break;
            case GameState.finish:
                break;
            case GameState.lose:
                break;
        }
    }
    private void Init()
    {
        hasAudio = (SaveSystem.GetInt("Audio", 1) == 1);
        hasVibration = (SaveSystem.GetInt("Vibration", 1) == 1);
        audioSource = GetComponent<AudioSource>();
        _AudioOnObj.SetActive(hasAudio);
        _AudioOffObj.SetActive(!hasAudio);
        _VibrationOnObj.SetActive(hasVibration);
        _VibrationOffObj.SetActive(!hasVibration);
        Vibration.Init();
    }
    public static void PlaySound(AudioClip clip)
    {
        if(hasAudio)
            audioSource.PlayOneShot(clip);
    }
    public void SetAudio(bool value)
    {
        hasAudio = value;
        SaveSystem.Save("Audio", hasAudio ? 1 : 0);
    }
    public void SetVibration(bool value)
    {
        hasVibration = value;
        SaveSystem.Save("Vibration", hasVibration ? 1 : 0);
    }
    public static void Vibrate(int milliseconds) {
        Vibration.Vibrate(milliseconds);
    }
}