using UnityEngine;

[CreateAssetMenu(fileName = "PlayersWeapon", menuName = "SO/PlayersWeapon", order = 51)]
public class WeaponSO : ScriptableObject
{
    [SerializeField] private float _damageValue;
    [SerializeField] private float _attackSpeedDelay;
    [SerializeField] private float _attackRange;
    [SerializeField] private float _ultMaxCharge;
    public float DamageValue=>_damageValue;
    public float AttackSpeedDelay=>_attackSpeedDelay;
    public float AttackRange => _attackRange;
    public float UltMaxCharge => _ultMaxCharge;
}