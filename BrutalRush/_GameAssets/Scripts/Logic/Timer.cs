using System;
using UnityEngine;
public class Timer : IEvent
{
	public Action callBack = delegate { };
	private float finishTime;
	private float timer;
	public Type GetTyper { get; }
	public bool IsRunning { get; set; }
	public Timer(Action callBack, float finishTime)
	{
		this.callBack = callBack;
		this.finishTime = finishTime;
		ManagerUpdate.AddUpdate(UpdateTick);
		IsRunning = true;
	}
	public void Restart(Action callBack = null, float finishTime = 0.0f)
	{
		if (finishTime > 0)
			this.finishTime = finishTime;

		if (callBack != null)
			this.callBack = callBack;

		if (timer > 0)
		{
			timer = 0.0f;
			IsRunning = true;
			return;
		}

		timer = 0.0f;
		IsRunning = true;

		ManagerUpdate.AddUpdate(UpdateTick);
	}
	public void Dispose()
	{
		ManagerUpdate.RemoveUpdate(UpdateTick);
		callBack = delegate { };
	}
	public void UpdateTick()
	{
		if (!IsRunning) return;
		Execute();
	}
	public void Execute()
	{
		timer = Mathf.Min(timer + Time.deltaTime, finishTime);
		if (timer < finishTime) return;
		timer = 0.0f;
		IsRunning = false;
		ManagerUpdate.RemoveUpdate(UpdateTick);
		callBack();
	}
}
