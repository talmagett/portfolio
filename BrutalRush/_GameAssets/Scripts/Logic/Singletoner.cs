using UnityEngine;

namespace Toolboxes
{
	public class Singletoner<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T _instance;
		private static Object _lock = new Object();
		public static T Singleton
		{
			get
			{
				lock (_lock)
				{
					if (_instance == null)
					{
						_instance = FindObjectOfType<T>();

						if (_instance == null)
						{
							var singleton = new GameObject("[SINGLETON]" + typeof(T));
							_instance = singleton.AddComponent<T>();
							DontDestroyOnLoad(singleton);
						}
					}
					return _instance;
				}
			}
		}
	}
}