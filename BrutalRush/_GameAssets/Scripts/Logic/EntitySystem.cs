using System.Collections;
using System.Collections.Generic;
using Toolboxes;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;
using DG.Tweening;
public class EntitySystem : Singletoner<EntitySystem>
{
    [SerializeField] private ManagerUpdate _managerUpdate;
    internal static PlayerController playerController;
    internal static StaticData levelDataContainer;
    internal static Boss levelBoss;
    internal static UIManager uiManager;
    internal static Camera mainCamera;
    internal static AudioManager audioManager;
    //internal static CinemachineVirtualCamera cinemachine;
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void ResetInit()
    {
        mainCamera = null;
        playerController = null;
        levelDataContainer = null;
        levelBoss = null;
        uiManager = null;
        audioManager = null;
        //cinemachine = null;
    }
    void Start()
    {
        mainCamera = FindObjectOfType<Camera>();
        levelDataContainer = FindObjectOfType<StaticData>();
        uiManager = FindObjectOfType<UIManager>();
        playerController = FindObjectOfType<PlayerController>();
        levelBoss = FindObjectOfType<Boss>();
        audioManager=FindObjectOfType<AudioManager>();
        //cinemachine = FindObjectOfType<CinemachineVirtualCamera>();
        Phases.SetGameStage(GameState.init);
    }
    private void Update()
    {
        if (Time.timeScale == 0) return;
        _managerUpdate.Update();
    }
    public void SetTargetForCamera(Transform target,float duration) {
        mainCamera.transform.SetParent(target);
        StartCoroutine(Rotating(target, duration));
        /*cinemachine.Follow = transform;
        cinemachine.LookAt = transform;
        var transposer = cinemachine.GetCinemachineComponent<CinemachineTransposer>();
        Vector3 followOffset = transposer.m_FollowOffset;
        followOffset.z *= -2;
        transposer.m_FollowOffset = followOffset;
        followOffset *= 2f;
        DOTween.To(() => transposer.m_FollowOffset, followOffset => transposer.m_FollowOffset = followOffset, followOffset, 1);*/
    }

    private IEnumerator Rotating(Transform target, float duration) {
        float deltaAngle = 0.1f;
        float dur = 3f;
        float deltaDist=20f;
        float angleY = mainCamera.transform.localEulerAngles.y > 0 ? mainCamera.transform.localEulerAngles.y : mainCamera.transform.localEulerAngles.y + 360;
        mainCamera.transform.DOShakePosition(0.2f);

        yield return new WaitForSeconds(0.2f);
        while (dur>=0) {
            Vector3 direction = (target.position - mainCamera.transform.position).normalized;
            mainCamera.transform.position -= direction*Time.deltaTime* deltaDist;
            
            mainCamera.transform.RotateAround(target.position,Vector3.up, angleY * Time.deltaTime);
            angleY += deltaAngle;
            if (dur <= 2) {
                angleY *= 0.99f;
                deltaDist *= 0.9f;
            }
            dur -= Time.deltaTime;
            yield return null;
        }
    }
    public static void ShakeCamera(float duration,float strength, int vibrato,float randomness) {
        mainCamera.transform.DOShakePosition(duration,strength,vibrato,randomness);
    }
    static bool isTimeSlowed = false;
    public static void TimeFreeze(float duration,float power) {
        if (isTimeSlowed) return;
        isTimeSlowed = true;
        Time.timeScale = power;
        Singleton.StartCoroutine(Singleton.TimeFreezeCounter(duration));
    }
    private IEnumerator TimeFreezeCounter(float duration)
    {
        yield return new WaitForSeconds(duration);
        if (isTimeSlowed){
            isTimeSlowed = false;
            Time.timeScale = 1;
        }
    }
    public void PauseGame() {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;
    }
    public void StartBattle()
    {
        Phases.SetGameStage(GameState.battle);
    }
    public void StartBossFight() {
        Phases.SetGameStage(GameState.bossDuel);
        //cinemachine.enabled = false;
    }
    public static class Phases
    {
        public static event System.Action<GameState> onGameStateChange;
        public static void SetGameStage(GameState gameState)
        {
            onGameStateChange?.Invoke(gameState);
        }
    }
    public void OnDisable()
    {
        ManagerUpdate.Clear();
    }
}
public enum GameState
{
    init,
    battle,
    bossDuel,
    finish,
    lose
}