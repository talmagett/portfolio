using System;
public interface IEvent : IDisposable
{
	Type GetTyper { get; }
	bool IsRunning { get; }
	void Execute();
}
interface IUltable
{
    abstract void Ultimate();
}

interface IAttackable
{
    void Attack(float charge);
}

interface IHitable
{
    void TakeDamage(int dmgValue);
    void Die();
}