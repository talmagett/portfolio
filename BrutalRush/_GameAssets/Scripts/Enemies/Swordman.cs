using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Swordman : Enemy
{
    public override void Spawned()
    {
        Init(); Battle();
    }
    protected override void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.init: Init(); break;
            case GameState.battle:
                Battle();
                break;
            case GameState.bossDuel:
                _agent.isStopped = true;
                _collider.enabled = false;

                isDead = true;
                PoolSystem.AddToPool(Name, this);
                gameObject.SetActive(false);
                ManagerUpdate.RemoveUpdate(ChasePlayer);
                break;
            case GameState.lose:
                _agent.isStopped = true;
                _collider.enabled = false;
                ManagerUpdate.RemoveUpdate(ChasePlayer);
                break;
        }
    }
    public override void Init()
    {
        _agent = GetComponent<NavMeshAgent>();
        _collider = GetComponent<Collider>();
        _health = _maxHealth;

        isDead = false;
        _agent.isStopped = false;
        _collider.enabled = true;
        canAttack = true;
    }
    private void Battle()
    {
        ManagerUpdate.AddUpdate(ChasePlayer);
    }
    private void ChasePlayer()
    {
        if (!isDead)
        {
            if (canAttack)
            {
                if (Vector3.Distance(transform.position, EntitySystem.playerController.transform.position) < _attackRange)
                {
                    canAttack = false;
                    _animator.SetTrigger("Attack");
                    StartCoroutine(AttackCooldown());
                }
                _agent.isStopped = false;
                _agent.SetDestination(EntitySystem.playerController.transform.position);
                _animator.SetBool("isMoving", canAttack);
            }
        }
    }
    public override void TakeDamage(int damageValue,float power)
    {
        _health -= damageValue;
        _agent.isStopped = true;
        _animator.SetTrigger("getHit");
        canAttack = false;

        if (_health <= 0) Die();
        else {
           StartCoroutine( GetRecoiled(power));
        }
    }
    public override IEnumerator GetRecoiled(float power)
    {
        Vector3 dir = (EntitySystem.playerController.transform.position - transform.position).normalized;
        for (int i = 0; i < 10; i++)
        {
            transform.position -= dir * Time.deltaTime * power;
            yield return null;
        }
        canAttack = true;
    }
    public override void Die()
    {
        EntitySystem.playerController.RemoveEnemyFromList(this);
        isDead = true;
        _agent.isStopped = true;
        _collider.enabled = false;
        var dieEffect= Instantiate(_dieEffect, transform.position, Quaternion.identity).main;
        dieEffect.startColor = _meshRenderer.material.color;
        AudioManager.PlaySound(_dieSound);
        PoolSystem.AddToPool(Name,this);
        gameObject.SetActive(false);
    }
    private IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        if (Vector3.Distance(transform.position, EntitySystem.playerController.transform.position) < _attackRange)
        {
            AudioManager.PlaySound(_attackSound);
            EntitySystem.playerController.Recoil(2,transform.position);
            EntitySystem.playerController.TakeDamage(_damage);
        }
        yield return new WaitForSeconds(_attackTimer);
        canAttack = true;
    }
}