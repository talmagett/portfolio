using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public abstract class Enemy : Character
{
    public string Name;
    [SerializeField] protected float _attackRange;
    [SerializeField] protected float _attackTimer;
    [SerializeField] protected int _damage;
    [SerializeField] protected Animator _animator;
    [SerializeField] protected ParticleSystem _dieEffect;
    [SerializeField] protected AudioClip _attackSound;
    [SerializeField] protected AudioClip _dieSound;
    [SerializeField] protected SkinnedMeshRenderer _meshRenderer;
    protected NavMeshAgent _agent;
    protected Collider _collider;
    protected bool canAttack = true;

    void OnEnable()
    {
        EntitySystem.Phases.onGameStateChange += GamePhaseChange;
    }
    void OnDisable()
    {
        EntitySystem.Phases.onGameStateChange -= GamePhaseChange;
    }
    protected abstract void GamePhaseChange(GameState gameState);   
    
    public abstract void Spawned();

    public void Recoil(float power)
    {
        StartCoroutine(GetRecoiled(power));
    }
    public abstract IEnumerator GetRecoiled(float power);
}