using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;
public class BossDetector : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _virtualCamera;
    [SerializeField] private Transform _changingData;
    public void ChangeCameraView() {
        CinemachineTransposer transposer = _virtualCamera.GetCinemachineComponent<CinemachineTransposer>();
        //Vector3 followOffset = transposer.m_FollowOffset;
        //transposer.m_FollowOffset = _changingData.localPosition;
        _virtualCamera.transform.DORotate(_changingData.localEulerAngles,2);
        DOTween.To(() => transposer.m_FollowOffset, x => transposer.m_FollowOffset = x, _changingData.localPosition, 2);
    }
}