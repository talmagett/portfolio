using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Boss : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] AudioClip _roar;
    private Transform[] _gates;
    private int gateIndex;
    public void Spawned() {
        transform.GetChild(0).gameObject.SetActive(true);
        _gates = new Transform[EntitySystem.levelDataContainer.Mulpiply.childCount];
        for (int i = 0; i < EntitySystem.levelDataContainer.Mulpiply.childCount; i++)
        {
            _gates[i] = EntitySystem.levelDataContainer.Mulpiply.GetChild(i);
        }
    }
    public void SetPushingPower(float pushingPower) {
        if (pushingPower > _gates.Length - 1) pushingPower = _gates.Length - 1;
        else if (pushingPower <= 0) pushingPower = 0;
        gateIndex = (int)pushingPower;
        
        TakeDamage();
    }
    public void TakeDamage() {
        AudioManager.PlaySound(_roar);
        _animator.SetTrigger("isDead");
        EntitySystem.Singleton.SetTargetForCamera(transform, 0.1f * gateIndex);
        Invoke("Fatality",0.2f);
    }
    private void Fatality() {
        print(gateIndex);
        transform.DOMove(_gates[gateIndex].position, 0.1f* gateIndex).SetEase(Ease.OutSine).OnComplete(()=> { 
            Invoke("OnDie", 1f);
        });
    }
    private void OnDie() {
        EntitySystem.Phases.SetGameStage(GameState.finish);
    }
}