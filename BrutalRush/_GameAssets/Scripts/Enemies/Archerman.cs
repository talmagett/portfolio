using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
public class Archerman : Enemy
{
    [SerializeField] private GameObject _arrow;
    public override void Spawned()
    {
        Init(); Battle();
    }
    protected override void GamePhaseChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.init: Init(); break;
            case GameState.battle:
                Battle();
                break;
            case GameState.bossDuel:
                _agent.isStopped = true;
                _collider.enabled = false;
                gameObject.SetActive(false);
                ManagerUpdate.RemoveUpdate(ChasePlayer);
                break;
            case GameState.lose:
                _agent.isStopped = true;
                _collider.enabled = false;
                ManagerUpdate.RemoveUpdate(ChasePlayer);
                break;
        }
    }
    public override void Init()
    {
        _agent = GetComponent<NavMeshAgent>();
        _collider = GetComponent<Collider>();
        _health = _maxHealth;

        isDead = false;
        _agent.isStopped = false;
        _collider.enabled = true;
        canAttack = true;
    }
    private void Battle()
    {
        ManagerUpdate.AddUpdate(ChasePlayer);
    }
    private void ChasePlayer()
    {
        if (!isDead)
        {
            if (canAttack)
            {
                if (Vector3.Distance(transform.position, EntitySystem.playerController.transform.position) < _attackRange)
                {
                    canAttack = false;
                    _agent.isStopped = true;
                    _animator.SetTrigger("Attack");
                    StartCoroutine(AttackCooldown());
                    _animator.SetBool("isMoving", canAttack);
                }
                else
                {
                    _agent.isStopped = false;
                    _agent.SetDestination(EntitySystem.playerController.transform.position);
                    _animator.SetBool("isMoving", canAttack);
                }
            }
        }
    }
    public override void TakeDamage(int damageValue, float power)
    {
        _health -= damageValue;
        _agent.isStopped = true;
        _animator.SetTrigger("getHit");
        canAttack = false;

        if (_health <= 0) Die();
        else {
            StartCoroutine(GetRecoiled(power));
        }
    }
    public override IEnumerator GetRecoiled(float power)
    {
        Vector3 dir = (EntitySystem.playerController.transform.position - transform.position).normalized;
        for (int i = 0; i < 10; i++)
        {
            transform.position -= dir * Time.deltaTime * power;
            yield return null;
        }
        canAttack = true;
    }
    public override void Die()
    {
        EntitySystem.playerController.RemoveEnemyFromList(this);
        isDead = true;
        _agent.isStopped = true;
        _collider.enabled = false;
        var dieEffect = Instantiate(_dieEffect, transform.position, Quaternion.identity).main;
        dieEffect.startColor = _meshRenderer.material.color;
        AudioManager.PlaySound(_dieSound);
        PoolSystem.AddToPool(Name, this);
        gameObject.SetActive(false);
    }
    private IEnumerator AttackCooldown()
    {
        transform.LookAt(EntitySystem.playerController.transform.position);
        Vector3 rotation = transform.eulerAngles;
        rotation.x -= 90;
        Transform arrowTr= Instantiate(_arrow,transform.position,Quaternion.Euler( rotation)).transform;
        Destroy(arrowTr.gameObject,2);
        arrowTr.DOJump(EntitySystem.playerController.transform.position, 1, 1, 0.8f).SetEase(Ease.Linear)
            .OnComplete(() => {
            if (Vector3.Distance(arrowTr.position, EntitySystem.playerController.transform.position) < 0.05f)
            {
                AudioManager.PlaySound(_attackSound);
                EntitySystem.playerController.TakeDamage(_damage);
                EntitySystem.playerController.Recoil(1, transform.position);
                Destroy(arrowTr.gameObject);
            }
        });
        yield return new WaitForSeconds(_attackTimer);
        canAttack = true;
    }
}